!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing' stamp: 'JDS 4/14/2022 14:44:34'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	| customerBook |
	
	customerBook := CustomerBook new.
	
	self 	
		should: [	customerBook addCustomerNamed: 'John Lennon'.] 
		notTakeMoreThan: (50 * millisecond).
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JDS 4/14/2022 14:44:48'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| customerBook paulMcCartney |
	

	paulMcCartney := 'Paul McCartney'.
	customerBook := self createCustomerBookThenAdd: paulMcCartney .
	
	self  
		should: [customerBook removeCustomerNamed: paulMcCartney.] 
		notTakeMoreThan: (100 * millisecond).
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JDS 4/16/2022 11:28:39'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook |
			
	customerBook := CustomerBook new.
	
	self  
		should: [	customerBook addCustomerNamed: ''.] 
		raise: Error 
		withExceptionDo: [ :anError | 
			self assert: customerBook customerCanNotBeEmptyErrorMessage equals: anError messageText.
			self assert: customerBook isEmpty ].! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JDS 4/16/2022 11:35:21'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
			
	johnLennon := 'John Lennon'.
	customerBook _ self createCustomerBookThenAdd: johnLennon.
	
	self  
		should: [ customerBook removeCustomerNamed: 'Paul McCartney' ]
		raise: NotFound 
		withExceptionDo: [ :anError | 
			self assert: 1 equals: customerBook numberOfCustomers.
			self assert: (customerBook includesCustomerNamed: johnLennon) ].
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JDS 4/14/2022 16:05:19'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	paulMcCartney := 'Paul McCartney'.
	customerBook _ self createCustomerBookThenAddAndSuspend: paulMcCartney.
	
	self assert: customerBook hasOnlySuspendedCustomersForATotalOf: 1.
	self assert: (customerBook includesCustomerNamed: paulMcCartney).
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JDS 4/14/2022 16:05:19'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	

	paulMcCartney := 'Paul McCartney'.
	customerBook _ self createCustomerBookThenAddAndSuspend: paulMcCartney.	

	customerBook removeCustomerNamed: paulMcCartney.
	
	self assert: customerBook hasOnlySuspendedCustomersForATotalOf: 0.
	self deny: (customerBook includesCustomerNamed: paulMcCartney).


	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JDS 4/16/2022 11:41:03'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook johnLennon |		
	
	johnLennon := 'John Lennon'.
	customerBook := self createCustomerBookThenAdd: johnLennon.
	
	self 	
		assert: customerBook 
		includesExactly: johnLennon 
		afterRaiseACantSuspendExceptionForSuspending: 'George Harrison'.! !

!CustomerBookTest methodsFor: 'testing' stamp: 'JDS 4/16/2022 11:41:03'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon |
			
	johnLennon := 'John Lennon'.
	customerBook := self createCustomerBookThenAddAndSuspend: johnLennon.
	
	self  
		assert: customerBook 
		includesExactly: johnLennon 
		afterRaiseACantSuspendExceptionForSuspending: johnLennon.
! !


!CustomerBookTest methodsFor: 'asserting' stamp: 'JDS 4/14/2022 16:05:38'!
assert: aCustomerBook hasOnlySuspendedCustomersForATotalOf: anAmount

	self assert: 0 equals: aCustomerBook numberOfActiveCustomers.
	self assert: anAmount equals: aCustomerBook numberOfSuspendedCustomers.
	self assert: anAmount equals: aCustomerBook numberOfCustomers.! !

!CustomerBookTest methodsFor: 'asserting' stamp: 'JDS 4/16/2022 11:41:03'!
assert: aCustomerBook includesExactly: anActiveCustomer afterRaiseACantSuspendExceptionForSuspending: aCustomer

	^self  
		should: [ aCustomerBook suspendCustomerNamed: aCustomer ]
		raise: CantSuspend 
		withExceptionDo: [ :anError | 
			self assert: 1 equals: aCustomerBook numberOfCustomers.
			self assert: (aCustomerBook includesCustomerNamed: anActiveCustomer) ]! !

!CustomerBookTest methodsFor: 'asserting' stamp: 'JDS 4/11/2022 21:40:35'!
should: aBlockClosure notTakeMoreThan: durationInMilliSeconds

	| millisecondsAfterRunning millisecondsBeforeRunning |

	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	aBlockClosure value.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.
	
	self assert: (millisecondsAfterRunning-millisecondsBeforeRunning) < durationInMilliSeconds.! !

!CustomerBookTest methodsFor: 'asserting' stamp: 'JDS 4/16/2022 11:28:39'!
should: aBlock raise: anExceptionHandlingCondition withExceptionDo: assertionsBlock

	[ aBlock value.
	self fail ]
		on: anExceptionHandlingCondition 
		do: assertionsBlock ! !


!CustomerBookTest methodsFor: 'instance creation' stamp: 'JDS 4/14/2022 11:41:47'!
createCustomerBookThenAdd: aCustomer	
	
	| customerBook |
	
	customerBook := CustomerBook new.
	customerBook addCustomerNamed: aCustomer.
	^ customerBook! !

!CustomerBookTest methodsFor: 'instance creation' stamp: 'JDS 4/14/2022 11:41:59'!
createCustomerBookThenAddAndSuspend: aCustomer	
	
	| customerBook |
	
	customerBook := self createCustomerBookThenAdd: aCustomer.
	customerBook suspendCustomerNamed: aCustomer.
	^ customerBook! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'error signal' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'error signal' stamp: 'JDS 4/16/2022 12:12:48'!
signalCustomerCanNotBeSuspended

	^ CantSuspend signal! !

!CustomerBook methodsFor: 'error signal' stamp: 'JDS 4/14/2022 21:13:03'!
signalCustomerNameCannotBeEmpty 

	self error: self customerCanNotBeEmptyErrorMessage ! !


!CustomerBook methodsFor: 'asserting' stamp: 'JDS 4/16/2022 12:12:07'!
assertCustomerIsIncluded: aName

	(active includes: aName) ifFalse: [self signalCustomerCanNotBeSuspended].! !

!CustomerBook methodsFor: 'asserting' stamp: 'JDS 4/16/2022 11:51:10'!
assertCustomerIsNotIncluded: aName

	^ (self includesCustomerNamed: aName) ifTrue: [ self signalCustomerAlreadyExists ]! !

!CustomerBook methodsFor: 'asserting' stamp: 'JDS 4/16/2022 11:48:59'!
assertNameCanNotBeEmpty: aName

	^ aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ]! !


!CustomerBook methodsFor: 'customer management' stamp: 'JDS 4/16/2022 11:51:10'!
addCustomerNamed: aName

	self assertNameCanNotBeEmpty: aName.
	self assertCustomerIsNotIncluded: aName.
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfCustomers
	
	^active size + suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'JDS 4/16/2022 11:58:44'!
removeCustomerNamed: aName 
 
	^ self 	removeItem: aName from: active ifAbsent: [
			self removeItem: aName from: suspended ifAbsent: [ NotFound signal ]].! !

!CustomerBook methodsFor: 'customer management' stamp: 'JDS 4/16/2022 12:07:55'!
removeItem: anItem from: aCollection ifAbsent: aBlock

	"Por requisitos de este TP copiamos y abstra�mos este c�digo el cual es equivalente 
	 al m�todo de Ordered Collection >> remove:ifAbsent: "

	1 to: aCollection size do: [ :anIndex |
			anItem = (aCollection at: anIndex) ifTrue: [
				aCollection removeAt: anIndex.
				^ anItem ]].
	^ aBlock value.! !

!CustomerBook methodsFor: 'customer management' stamp: 'JDS 4/16/2022 12:10:34'!
suspendCustomerNamed: aName 
	
	self assertCustomerIsIncluded: aName.
	
	active remove: aName.
	suspended add: aName
! !


!CustomerBook methodsFor: 'error message' stamp: 'JDS 4/14/2022 21:12:35'!
customerCanNotBeEmptyErrorMessage
	
	^ self class customerCanNotBeEmptyErrorMessage! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/11/2022 07:18:12'!
customerAlreadyExistsErrorMessage

	^'Customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 4/11/2022 07:18:16'!
customerCanNotBeEmptyErrorMessage

	^'Customer name cannot be empty!!!!!!'! !
