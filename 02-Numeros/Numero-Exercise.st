!classDefinition: #NumeroTest category: 'Numero-Exercise'!
TestCase subclass: #NumeroTest
	instanceVariableNames: 'zero one two four oneFifth oneHalf five twoFifth twoTwentyfifth fiveHalfs three eight negativeOne negativeTwo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:11'!
test01isCeroReturnsTrueWhenAskToZero

	self assert: zero isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:12'!
test02isCeroReturnsFalseWhenAskToOthersButZero

	self deny: one isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test03isOneReturnsTrueWhenAskToOne

	self assert: one isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test04isOneReturnsFalseWhenAskToOtherThanOne

	self deny: zero isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:14'!
test05EnteroAddsWithEnteroCorrectly

	self assert: one + one equals: two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:18'!
test06EnteroMultipliesWithEnteroCorrectly

	self assert: two * two equals: four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:20'!
test07EnteroDividesEnteroCorrectly

	self assert: two / two equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:38'!
test08FraccionAddsWithFraccionCorrectly
"
    La suma de fracciones es:
	 
	a/b + c/d = (a.d + c.b) / (b.d)
	 
	SI ESTAN PENSANDO EN LA REDUCCION DE FRACCIONES NO SE PREOCUPEN!!
	TODAVIA NO SE ESTA TESTEANDO ESE CASO
"
	| sevenTenths |

	sevenTenths := (Entero with: 7) / (Entero with: 10).

	self assert: oneFifth + oneHalf equals: sevenTenths! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:52'!
test09FraccionMultipliesWithFraccionCorrectly
"
    La multiplicacion de fracciones es:
	 
	(a/b) * (c/d) = (a.c) / (b.d)
"

	self assert: oneFifth * twoFifth equals: twoTwentyfifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:56'!
test10FraccionDividesFraccionCorrectly
"
    La division de fracciones es:
	 
	(a/b) / (c/d) = (a.d) / (b.c)
"

	self assert: oneHalf / oneFifth equals: fiveHalfs! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test11EnteroAddsFraccionCorrectly
"
	Ahora empieza la diversion!!
"

	self assert: one + oneFifth equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test12FraccionAddsEnteroCorrectly

	self assert: oneFifth + one equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:50'!
test13EnteroMultipliesFraccionCorrectly

	self assert: two * oneFifth equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:52'!
test14FraccionMultipliesEnteroCorrectly

	self assert: oneFifth * two equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:57'!
test15EnteroDividesFraccionCorrectly

	self assert: one / twoFifth equals: fiveHalfs  ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:59'!
test16FraccionDividesEnteroCorrectly

	self assert: twoFifth / five equals: twoTwentyfifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:38'!
test17AFraccionCanBeEqualToAnEntero

	self assert: two equals: four / two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:39'!
test18AparentFraccionesAreEqual

	self assert: oneHalf equals: two / four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:40'!
test19AddingFraccionesCanReturnAnEntero

	self assert: oneHalf + oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test20MultiplyingFraccionesCanReturnAnEntero

	self assert: (two/five) * (five/two) equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test21DividingFraccionesCanReturnAnEntero

	self assert: oneHalf / oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:43'!
test22DividingEnterosCanReturnAFraccion

	self assert: two / four equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test23CanNotDivideEnteroByZero

	self 
		should: [ one / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test24CanNotDivideFraccionByZero

	self 
		should: [ oneHalf / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test25AFraccionCanNotBeZero

	self deny: oneHalf isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test26AFraccionCanNotBeOne

	self deny: oneHalf isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 4/15/2021 16:45:35'!
test27EnteroSubstractsEnteroCorrectly

	self assert: four - one equals: three! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:47:41'!
test28FraccionSubstractsFraccionCorrectly
	
	self assert: twoFifth - oneFifth equals: oneFifth.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:00'!
test29EnteroSubstractsFraccionCorrectly

	self assert: one - oneHalf equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:05'!
test30FraccionSubstractsEnteroCorrectly

	| sixFifth |
	
	sixFifth := (Entero with: 6) / (Entero with: 5).
	
	self assert: sixFifth - one equals: oneFifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:08'!
test31SubstractingFraccionesCanReturnAnEntero

	| threeHalfs |
	
	threeHalfs := (Entero with: 3) / (Entero with: 2).
	
	self assert: threeHalfs - oneHalf equals: one.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:48'!
test32SubstractingSameEnterosReturnsZero

	self assert: one - one equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:01'!
test33SubstractingSameFraccionesReturnsZero

	self assert: oneHalf - oneHalf equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:14'!
test34SubstractingAHigherValueToANumberReturnsANegativeNumber

	| negativeThreeHalfs |
	
	negativeThreeHalfs := (Entero with: -3) / (Entero with: 2).	

	self assert: one - fiveHalfs equals: negativeThreeHalfs.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:23'!
test35FibonacciZeroIsOne

	self assert: zero fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:32'!
test36FibonacciOneIsOne

	self assert: one fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:39'!
test37FibonacciEnteroReturnsAddingPreviousTwoFibonacciEnteros

	self assert: four fibonacci equals: five.
	self assert: three fibonacci equals: three. 
	self assert: five fibonacci equals: four fibonacci + three fibonacci.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:47'!
test38FibonacciNotDefinedForNegativeNumbers

	self 
		should: [negativeOne fibonacci]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Entero negativeFibonacciErrorDescription ].! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:55'!
test39NegationOfEnteroIsCorrect

	self assert: two negated equals: negativeTwo.
		! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:03'!
test40NegationOfFraccionIsCorrect

	self assert: oneHalf negated equals: negativeOne / two.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:11'!
test41SignIsCorrectlyAssignedToFractionWithTwoNegatives

	self assert: oneHalf equals: (negativeOne / negativeTwo)! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:17'!
test42SignIsCorrectlyAssignedToFractionWithNegativeDivisor

	self assert: oneHalf negated equals: (one / negativeTwo)! !


!NumeroTest methodsFor: 'setup' stamp: 'NR 9/23/2018 23:46:28'!
setUp

	zero := Entero with: 0.
	one := Entero with: 1.
	two := Entero with: 2.
	three:= Entero with: 3.
	four := Entero with: 4.
	five := Entero with: 5.
	eight := Entero with: 8.
	negativeOne := Entero with: -1.
	negativeTwo := Entero with: -2.
	
	oneHalf := one / two.
	oneFifth := one / five.
	twoFifth := two / five.
	twoTwentyfifth := two / (Entero with: 25).
	fiveHalfs := five / two.
	! !


!classDefinition: #Numero category: 'Numero-Exercise'!
Object subclass: #Numero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
* aMultiplier

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
+ anAdder

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 22:21:28'!
- aSubtrahend

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
/ aDivisor

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 23:37:13'!
negated
	
	^self * (Entero with: -1)! !


!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isOne

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isZero

	self subclassResponsibility ! !


!Numero methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 08:03:31'!
addToEntero: anEnteroBeingAddedTo 

	self subclassResponsibility ! !

!Numero methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 08:04:23'!
addToFraccion: aFraccionToAddTo 

	self subclassResponsibility ! !

!Numero methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 08:05:12'!
divideAFraccion: aFraccionToDivide	

	self subclassResponsibility ! !

!Numero methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 08:07:08'!
divideAnEntero: anEnteroBeingDivided

	self subclassResponsibility ! !

!Numero methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 08:07:29'!
multiplyByAFraccion: aFraccionBeingMultiplied 

	self subclassResponsibility ! !

!Numero methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 08:07:46'!
multiplyByAnEntero: anEnteroBeingMultiplied

	self subclassResponsibility ! !

!Numero methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 08:08:04'!
substractFromEntero: anEnteroToSubstractFrom 

	self subclassResponsibility ! !

!Numero methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 08:08:23'!
substractFromFraccion: aFraccionToSubstractFrom 

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Numero class' category: 'Numero-Exercise'!
Numero class
	instanceVariableNames: ''!

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:02'!
canNotDivideByZeroErrorDescription

	^'No se puede dividir por cero!!!!!!'! !


!classDefinition: #Entero category: 'Numero-Exercise'!
Numero subclass: #Entero
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Entero methodsFor: 'arithmetic operations' stamp: 'AEJDS 4/23/2022 20:47:17'!
* aMultiplier 

	^ aMultiplier multiplyByAnEntero: self.
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'AEJDS 4/22/2022 16:24:32'!
+ anAdder 

	^anAdder addToEntero: self.
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'AEJDS 4/23/2022 15:08:15'!
- aSubtrahend 

	^ aSubtrahend substractFromEntero: self .
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'AEJDS 4/23/2022 15:12:02'!
/ aDivisor 
	
	^aDivisor divideAnEntero: self.
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'AEJDS 4/23/2022 16:31:03'!
// aDivisor 
	
	^Entero with: value // aDivisor integerValue! !

!Entero methodsFor: 'arithmetic operations' stamp: 'AEJDS 4/23/2022 14:32:47'!
fibonacci

	self subclassResponsibility .
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'AEJDS 4/23/2022 13:49:37'!
greatestCommonDivisorWith: anEntero 
	
	^Entero with: (value gcd: anEntero integerValue)! !


!Entero methodsFor: 'comparing' stamp: 'AEJDS 4/23/2022 15:38:04'!
= anObject

	^(anObject isKindOf: self class) and: [ value = anObject integerValue ]! !

!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:17'!
hash

	^value hash! !


!Entero methodsFor: 'initialization' stamp: 'AEJDS 4/22/2022 19:20:34'!
initializeWith: aValue 
	
	value := aValue! !


!Entero methodsFor: 'value' stamp: 'HernanWilkinson 5/7/2016 21:02'!
integerValue

	"Usamos integerValue en vez de value para que no haya problemas con el mensaje value implementado en Object - Hernan"
	
	^value! !


!Entero methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:53:19'!
printOn: aStream

	aStream print: value ! !


!Entero methodsFor: 'private - arithmetic' stamp: 'AEJDS 4/23/2022 14:11:19'!
addToEntero: anEnteroBeingAddedTo 

	^Entero with: value + anEnteroBeingAddedTo integerValue.
	
	
	! !

!Entero methodsFor: 'private - arithmetic' stamp: 'AEJDS 4/23/2022 16:18:07'!
addToFraccion: aFraccionToAddTo 

	^ (aFraccionToAddTo denominator * self + aFraccionToAddTo numerator) / aFraccionToAddTo denominator .
		
! !

!Entero methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 09:20:13'!
divideACoprimeEntero: aCoprimeEnteroToBeDivided

	^Fraccion createWithCoprimes: aCoprimeEnteroToBeDivided over: self.
	! !

!Entero methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 08:04:52'!
divideAFraccion: aFraccionToDivide	

	^ (aFraccionToDivide numerator) / ( aFraccionToDivide denominator * self)
	! !

!Entero methodsFor: 'private - arithmetic' stamp: 'AEJDS 4/23/2022 20:31:51'!
divideAnEntero: anEnteroBeingDivided

	self subclassResponsibility .! !

!Entero methodsFor: 'private - arithmetic' stamp: 'AEJDS 4/23/2022 20:46:23'!
multiplyByAFraccion: aFraccionBeingMultiplied

	^(aFraccionBeingMultiplied numerator * self ) / aFraccionBeingMultiplied denominator .
	
		
! !

!Entero methodsFor: 'private - arithmetic' stamp: 'AEJDS 4/23/2022 20:47:17'!
multiplyByAnEntero: anEnteroBeingMultiplied
	
	^Entero with: value * anEnteroBeingMultiplied integerValue.
	! !

!Entero methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 08:29:09'!
substractFromEntero: anEnteroToSubstractFrom

	^Entero with: (anEnteroToSubstractFrom integerValue - value ).
	
! !

!Entero methodsFor: 'private - arithmetic' stamp: 'AEJDS 4/23/2022 15:12:17'!
substractFromFraccion: aFraccionToSubstractFrom 

	| denominator |
	denominator _ aFraccionToSubstractFrom denominator.
	^ aFraccionToSubstractFrom numerator - (self *  denominator) / denominator.
	! !


!Entero methodsFor: 'testing' stamp: 'AEJDS 4/23/2022 14:25:55'!
isOne
	self subclassResponsibility .
	! !

!Entero methodsFor: 'testing' stamp: 'AEJDS 4/23/2022 14:25:33'!
isZero
	
	self subclassResponsibility .
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Entero class' category: 'Numero-Exercise'!
Entero class
	instanceVariableNames: ''!

!Entero class methodsFor: 'instance creation' stamp: 'AEJDS 4/23/2022 20:04:22'!
with: aValue 
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	| enteroSubclass |
	aValue isInteger ifFalse: [  self error: 'aValue debe ser anInteger' ].
	
	enteroSubclass _ self subclasses detect: [ :class | class canHandle: aValue ].
	
	^enteroSubclass new initializeWith: aValue.
	
! !


!Entero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:24'!
negativeFibonacciErrorDescription
	^ ' Fibonacci no est� definido aqu� para Enteros Negativos!!!!!!'! !


!Entero class methodsFor: 'private - instance creation' stamp: 'AE-JDS 4/24/2022 07:31:29'!
canHandle: aValue

	self subclassResponsibility ! !


!classDefinition: #Cero category: 'Numero-Exercise'!
Entero subclass: #Cero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Cero methodsFor: 'private - arithmetic' stamp: 'AEJDS 4/23/2022 20:33:09'!
divideAnEntero: aEnteroToBeDivided

	self error: self class canNotDivideByZeroErrorDescription.
	
! !


!Cero methodsFor: 'testing' stamp: 'AEJDS 4/23/2022 14:28:12'!
isOne
	^false
	! !

!Cero methodsFor: 'testing' stamp: 'AEJDS 4/23/2022 14:28:05'!
isZero
	^true
	! !


!Cero methodsFor: 'arithmetic operations' stamp: 'AEJDS 4/22/2022 19:44:44'!
fibonacci
	
	^ Entero with: 1.
	
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cero class' category: 'Numero-Exercise'!
Cero class
	instanceVariableNames: ''!

!Cero class methodsFor: 'private - instance creation' stamp: 'AE-JDS 4/24/2022 07:31:13'!
canHandle: aValue

	^ aValue = 0.
	! !


!classDefinition: #EnteroNegativo category: 'Numero-Exercise'!
Entero subclass: #EnteroNegativo
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroNegativo methodsFor: 'arithmetic operations' stamp: 'AE-JDS 4/24/2022 07:25:30'!
divideAnEntero: anEnteroToDivide

	^ (anEnteroToDivide negated) / ( self negated)
	! !

!EnteroNegativo methodsFor: 'arithmetic operations' stamp: 'AE-JDS 4/24/2022 08:52:53'!
fibonacci

	self error: self class negativeFibonacciErrorDescription	
! !


!EnteroNegativo methodsFor: 'testing' stamp: 'AE-JDS 4/24/2022 08:52:27'!
isOne

	^ false
	! !

!EnteroNegativo methodsFor: 'testing' stamp: 'AE-JDS 4/24/2022 08:52:33'!
isZero

	^ false
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroNegativo class' category: 'Numero-Exercise'!
EnteroNegativo class
	instanceVariableNames: ''!

!EnteroNegativo class methodsFor: 'private - instance creation' stamp: 'AEJDS 4/23/2022 16:26:08'!
canHandle: aValue

	^ aValue < 0.
	! !


!classDefinition: #EnteroPositivoMayorQueUno category: 'Numero-Exercise'!
Entero subclass: #EnteroPositivoMayorQueUno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroPositivoMayorQueUno methodsFor: 'arithmetic operations' stamp: 'AE-JDS 4/24/2022 08:55:23'!
fibonacci

	| one two |
	
	one := Entero with: 1.
	two := Entero with: 2.
	
	^ (self - one) fibonacci + (self - two) fibonacci
		! !


!EnteroPositivoMayorQueUno methodsFor: 'testing' stamp: 'AEJDS 4/23/2022 14:28:40'!
isOne
	^false.
	! !

!EnteroPositivoMayorQueUno methodsFor: 'testing' stamp: 'AEJDS 4/23/2022 14:28:47'!
isZero
	^false.
	! !


!EnteroPositivoMayorQueUno methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 08:30:19'!
divideAnEntero: anEnteroBeingDivided

	| denominator greatestCommonDivisor numerator |
	greatestCommonDivisor := anEnteroBeingDivided greatestCommonDivisorWith: self. 
	numerator := anEnteroBeingDivided // greatestCommonDivisor.
	denominator := self // greatestCommonDivisor.
	
	^ denominator divideACoprimeEntero: numerator. 
	
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroPositivoMayorQueUno class' category: 'Numero-Exercise'!
EnteroPositivoMayorQueUno class
	instanceVariableNames: ''!

!EnteroPositivoMayorQueUno class methodsFor: 'private - instance creation' stamp: 'AEJDS 4/23/2022 16:27:26'!
canHandle: aValue
	^aValue > 1
	 ! !


!classDefinition: #Uno category: 'Numero-Exercise'!
Entero subclass: #Uno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Uno methodsFor: 'arithmetic operations' stamp: 'AEJDS 4/22/2022 19:43:21'!
fibonacci
	
	^ self.
! !


!Uno methodsFor: 'testing' stamp: 'AEJDS 4/23/2022 14:27:51'!
isOne
	^true
	! !

!Uno methodsFor: 'testing' stamp: 'AEJDS 4/23/2022 14:27:42'!
isZero
	^false
	! !


!Uno methodsFor: 'private - arithmetic' stamp: 'AEJDS 4/23/2022 16:19:58'!
divideACoprimeEntero: aCoprimeEnteroToBeDivided

	^aCoprimeEnteroToBeDivided ! !

!Uno methodsFor: 'private - arithmetic' stamp: 'AEJDS 4/23/2022 20:33:41'!
divideAnEntero: anEnteroToBeDivided

	^anEnteroToBeDivided ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Uno class' category: 'Numero-Exercise'!
Uno class
	instanceVariableNames: ''!

!Uno class methodsFor: 'private - instance creation' stamp: 'AE-JDS 4/24/2022 07:30:00'!
canHandle: aValue

	^ aValue = 1.
	! !


!classDefinition: #Fraccion category: 'Numero-Exercise'!
Numero subclass: #Fraccion
	instanceVariableNames: 'numerator denominator'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Fraccion methodsFor: 'arithmetic operations' stamp: 'AEJDS 4/23/2022 20:46:23'!
* aMultiplier 
	
	^aMultiplier multiplyByAFraccion: self.
 ! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'AEJDS 4/23/2022 14:58:17'!
+ anAdder 
	
	^anAdder addToFraccion: self.
! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'AEJDS 4/23/2022 15:12:17'!
- aSubtrahend 

	^aSubtrahend substractFromFraccion: self.
! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'AEJDS 4/23/2022 15:10:37'!
/ aDivisor 
	
	^aDivisor divideAFraccion: self.
! !


!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:42'!
= anObject

	^(anObject isKindOf: self class) and: [ (numerator * anObject denominator) = (denominator * anObject numerator) ]! !

!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:50'!
hash

	^(numerator hash / denominator hash) hash! !


!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
denominator

	^ denominator! !

!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
numerator

	^ numerator! !


!Fraccion methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 22:54'!
initializeWith: aNumerator over: aDenominator

	"Estas precondiciones estan por si se comenten errores en la implementacion - Hernan"
	aNumerator isZero ifTrue: [ self error: 'una fraccion no puede ser cero' ].
	aDenominator isOne ifTrue: [ self error: 'una fraccion no puede tener denominador 1 porque sino es un entero' ].
	
	numerator := aNumerator.
	denominator := aDenominator ! !


!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isOne
	
	^false! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isZero
	
	^false! !


!Fraccion methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:54:46'!
printOn: aStream

	aStream 
		print: numerator;
		nextPut: $/;
		print: denominator ! !


!Fraccion methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 07:06:26'!
addToEntero: anEnteroBeingAddedTo 

	^ (anEnteroBeingAddedTo * denominator + numerator) / denominator.
	! !

!Fraccion methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 07:24:13'!
addToFraccion: aFraccionToAddTo

	| newDenominator newNumerator |
	
	newNumerator := (aFraccionToAddTo numerator * denominator) + (aFraccionToAddTo denominator * numerator).
	newDenominator := aFraccionToAddTo denominator * denominator.
	
	^newNumerator / newDenominator 
! !

!Fraccion methodsFor: 'private - arithmetic' stamp: 'AEJDS 4/23/2022 15:10:36'!
divideAFraccion: aFraccionToDivide
	
	^(aFraccionToDivide numerator * denominator) / (aFraccionToDivide denominator * numerator).
		! !

!Fraccion methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 09:27:40'!
divideAnEntero: anEnteroBeingDivided
		
	^ (anEnteroBeingDivided * denominator) / numerator.
		! !

!Fraccion methodsFor: 'private - arithmetic' stamp: 'AEJDS 4/23/2022 20:46:23'!
multiplyByAFraccion: aFraccionBeingMultiplied 
	
	^(numerator * aFraccionBeingMultiplied numerator) / (denominator * aFraccionBeingMultiplied denominator) .
		
! !

!Fraccion methodsFor: 'private - arithmetic' stamp: 'AEJDS 4/23/2022 20:47:17'!
multiplyByAnEntero: anEnteroBeingMultiplied

	^anEnteroBeingMultiplied * numerator / denominator.
		! !

!Fraccion methodsFor: 'private - arithmetic' stamp: 'AEJDS 4/23/2022 15:10:06'!
substractFromEntero: anEnteroToSubstractFrom 

	^ (anEnteroToSubstractFrom * denominator - numerator ) / denominator 
	! !

!Fraccion methodsFor: 'private - arithmetic' stamp: 'AE-JDS 4/24/2022 07:24:50'!
substractFromFraccion: aFraccionToSubstractFrom 
	
	| newDenominator newNumerator |
	
	newNumerator := ( aFraccionToSubstractFrom numerator * denominator) - (aFraccionToSubstractFrom denominator *  numerator).
	newDenominator := denominator * aFraccionToSubstractFrom denominator.

	^newNumerator / newDenominator 

	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Fraccion class' category: 'Numero-Exercise'!
Fraccion class
	instanceVariableNames: ''!

!Fraccion class methodsFor: 'intance creation' stamp: 'AE-JDS 4/24/2022 09:20:13'!
createWithCoprimes: aDividend over: aDivisor

	^self new initializeWith: aDividend over: aDivisor.
	
! !
