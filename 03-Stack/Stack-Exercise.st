!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'AEJDS 4/28/2022 14:01:26'!
test01prefixCannotBeEmpty

	| sentences searcher |
	
	searcher _ SentenceFinderByPrefix new.
	sentences _ OOStack new.
	
	self should: 
		[ searcher findByPrefix: ''  in: sentences. ]
		raise: Error - MessageNotUnderstood 
		withMessageText: SentenceFinderByPrefix emptyPrefixErrorDescription.
		
		
		! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'AEJDS 4/28/2022 14:28:55'!
test02findAPrefixOnAnEmptyStackReturnsNothing

	| emptyStack searcher result |
	
	searcher _ SentenceFinderByPrefix new.
	emptyStack _ OOStack new.
	
	result _ searcher findByPrefix: 'a' in: emptyStack.
	
	self assert: result isEmpty.
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'AEJDS 4/28/2022 15:07:47'!
test03findAPrefixOnAStackWithExactlyOneMatchingSentenceReturnsAOneElementCollection

	| sentences searcher result matchingPrefix matchingSentence |
	
	searcher _ SentenceFinderByPrefix new.
	
	matchingSentence _ 'aaaa'.
	matchingPrefix _ 'a'.
	
	sentences _ OOStack new.
	sentences push: matchingSentence.
	
	result _ searcher findByPrefix: matchingPrefix in: sentences.
	
	self assert: 1 equals: result size.
	self assert: matchingSentence equals: result first.! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'AEJDS 4/28/2022 14:38:22'!
test04findAPrefixOnAStackWithExactlyOneNotMatchingSentenceReturnsAnEmptyCollection

	| sentences searcher result matchingPrefix matchingSentence |
	searcher _ SentenceFinderByPrefix new.
	
	matchingSentence _ 'aaaa'.
	matchingPrefix _ 'a'.
	
	sentences _ OOStack new.
	sentences push: matchingSentence.
	
	result _ searcher findByPrefix: matchingPrefix in: sentences.
	
	self assert: 1 equals: result size.
	self assert: matchingSentence equals: result first.
	
! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'AEJDS 4/28/2022 14:38:34'!
test05findAPrefixOnAStackWithMultipleNotMatchingSentencesReturnsAnEmptyCollection

	| sentences searcher result matchingPrefix |

	searcher _ SentenceFinderByPrefix new.

	matchingPrefix _ 'a'.
	
	sentences _ OOStack new.
	sentences push: 'b'.
	sentences push: 'bbbb'.
	sentences push: 'bbbb bb bb'.
	
	result _ searcher findByPrefix: matchingPrefix in: sentences.
	
	self assert: result isEmpty.
	
	
! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'AEJDS 4/28/2022 14:36:46'!
test06findAPrefixContainingSpacesMustRaiseAnError

	| searcher sentences prefix |
	searcher _ SentenceFinderByPrefix new.
	
	sentences _ OOStack new.
	prefix _ 'sad asdasd'.
	
	self should: 
		[ searcher findByPrefix: prefix  in: sentences. ]
		raise: Error - MessageNotUnderstood 
		withMessageText: SentenceFinderByPrefix prefixContainsSpaceErrorDescription .
		
		
! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'AEJDS 4/28/2022 14:43:41'!
test07findAPrefixOnAStackWithMultipleSentencesMatching

	| searcher sentences matchingPrefix matchingSentence1 matchingSentence2 result |
	searcher _ SentenceFinderByPrefix new.
	
	matchingPrefix _ 'acrg'.
	matchingSentence1 _ 'acrg'.
	matchingSentence2 _ 'acrg ja dg'.

	sentences _ OOStack new.
	
	sentences push: 'no va'.
	sentences push: 'no, tampoco'.
	sentences push: matchingSentence1 .
	sentences push: ' a c r g'.
	sentences push: matchingSentence2 .
	sentences push: ' ja dg acrg'.
	
	result _ searcher findByPrefix: matchingPrefix in: sentences .

	self assert: 2 equals: result size.
	self assert: (result includes: matchingSentence1).
	self assert: (result includes: matchingSentence2).! !

!SentenceFinderByPrefixTest methodsFor: 'testing' stamp: 'AEJDS 4/28/2022 14:41:30'!
test08findAPrefixOnAStackWithMultipleSentencesMatchesExactlyOneSentence

	| searcher sentences matchingPrefix matchingSentence result |
	searcher _ SentenceFinderByPrefix new.
	
	matchingPrefix _ 'Wint'.
	matchingSentence _ 'Winter is here'.
	
	sentences _ OOStack new.
	
	sentences push: 'winter is coming'.
	sentences push: 'winning is everything'.
	sentences push: matchingSentence .
	sentences push: 'The Winds of Winter'.
	
	result _ searcher findByPrefix: matchingPrefix in: sentences .
	
	self assert: 1 equals: result size.
	self assert: (result includes: matchingSentence).! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'contents state'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'initialization' stamp: 'JDS 4/26/2022 22:25:12'!
initialize

	contents _ OrderedCollection new.
! !


!OOStack methodsFor: 'testing' stamp: 'JDS 4/26/2022 22:25:45'!
isEmpty
	
	^ contents isEmpty! !


!OOStack methodsFor: 'accessing' stamp: 'AE-JDS 4/25/2022 21:21:43'!
pop
	
	self top.
	^ contents removeLast.! !

!OOStack methodsFor: 'accessing' stamp: 'AE-JDS 4/25/2022 21:21:43'!
push: anObject 
	
	contents addLast: anObject. ! !

!OOStack methodsFor: 'accessing' stamp: 'AE-JDS 4/25/2022 21:21:43'!
size
	
	^contents size! !

!OOStack methodsFor: 'accessing' stamp: 'JDS 4/27/2022 06:03:55'!
top
	
	^ (OOStackState for: self) top.
! !


!OOStack methodsFor: 'private' stamp: 'JDS 4/27/2022 06:05:59'!
safeTop

	^ contents last! !


!OOStack methodsFor: 'error signal' stamp: 'JDS 4/27/2022 06:04:45'!
signalEmptyStackError

	self error: self class stackEmptyErrorDescription! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/16/2021 17:39:43'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #OOStackState category: 'Stack-Exercise'!
Object subclass: #OOStackState
	instanceVariableNames: 'stack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackState methodsFor: 'initialization' stamp: 'JDS 4/27/2022 05:11:32'!
initializeFor: anOOStack

	stack _ anOOStack. ! !


!OOStackState methodsFor: 'accessing' stamp: 'JDS 4/27/2022 06:03:28'!
top

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStackState class' category: 'Stack-Exercise'!
OOStackState class
	instanceVariableNames: ''!

!OOStackState class methodsFor: 'instance creation' stamp: 'AEJDS 4/28/2022 14:09:18'!
for: anOOStack

	| stateSubclass |
	
	stateSubclass _ self allSubclasses detect: [ :aSubclass | aSubclass isAStackStateHandlerFor: anOOStack ].
	^ stateSubclass new initializeFor: anOOStack! !


!classDefinition: #OOStackEmptyState category: 'Stack-Exercise'!
OOStackState subclass: #OOStackEmptyState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackEmptyState methodsFor: 'accessing' stamp: 'JDS 4/27/2022 06:04:27'!
top

	stack signalEmptyStackError! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStackEmptyState class' category: 'Stack-Exercise'!
OOStackEmptyState class
	instanceVariableNames: ''!

!OOStackEmptyState class methodsFor: 'private' stamp: 'AEJDS 4/28/2022 14:09:18'!
isAStackStateHandlerFor: anOOStack

	^ anOOStack isEmpty! !


!classDefinition: #OOStackNotEmptyState category: 'Stack-Exercise'!
OOStackState subclass: #OOStackNotEmptyState
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackNotEmptyState methodsFor: 'accessing' stamp: 'JDS 4/27/2022 06:05:07'!
top

	^ stack safeTop! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStackNotEmptyState class' category: 'Stack-Exercise'!
OOStackNotEmptyState class
	instanceVariableNames: ''!

!OOStackNotEmptyState class methodsFor: 'private' stamp: 'AEJDS 4/28/2022 14:09:18'!
isAStackStateHandlerFor: anOOStack

	^ anOOStack isEmpty not! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: 'stack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'accessing' stamp: 'AEJDS 4/28/2022 15:01:11'!
findByPrefix: aPrefix in: aStackOfSentences 

	| matchingSentences temporalStack |
	
	self assertPrefixIsNotEmpty: aPrefix.
	self assertPrefixDoesNotContainSpaces: aPrefix.
	
	matchingSentences _ OrderedCollection new.
			
	temporalStack _ self addTo: matchingSentences sentences: aStackOfSentences withPrefix: aPrefix.
	
	self rebuildOriginalStack: aStackOfSentences with: temporalStack .
	
	^matchingSentences
	
	! !


!SentenceFinderByPrefix methodsFor: 'error signal' stamp: 'JDS 4/28/2022 13:47:04'!
signalEmptyPrefixError

	^ self error: self class emptyPrefixErrorDescription! !

!SentenceFinderByPrefix methodsFor: 'error signal' stamp: 'JDS 4/28/2022 13:46:39'!
signalPrefixContainsSpaceError

	^ self error: self class prefixContainsSpaceErrorDescription! !


!SentenceFinderByPrefix methodsFor: 'asserting' stamp: 'AEJDS 4/28/2022 15:00:52'!
assertPrefixDoesNotContainSpaces: aPrefix

	^ (aPrefix findString: ' ') > 0 ifTrue: [ self signalPrefixContainsSpaceError ]! !

!SentenceFinderByPrefix methodsFor: 'asserting' stamp: 'JDS 4/28/2022 13:47:04'!
assertPrefixIsNotEmpty: aPrefix

	^ (aPrefix isEmpty) ifTrue: [ self signalEmptyPrefixError ]! !


!SentenceFinderByPrefix methodsFor: 'private' stamp: 'AEJDS 4/28/2022 01:38:00'!
addTo: results if: aSentence isPrefixedWith: aPrefix 

	( aSentence beginsWith: aPrefix ) ifTrue:
		[ results add: aSentence ].
		
! !

!SentenceFinderByPrefix methodsFor: 'private' stamp: 'AEJDS 4/28/2022 15:00:35'!
addTo: matchingSentences sentences: aStackOfSentences withPrefix: aPrefix

	| temporalStack |
	
	temporalStack _ OOStack new.
		
	(aStackOfSentences size) timesRepeat: 
		[ | sentence |
			sentence _ aStackOfSentences pop.
			self addTo: matchingSentences if: sentence isPrefixedWith: aPrefix .
			temporalStack push: sentence.
		].

	^ temporalStack.
	! !

!SentenceFinderByPrefix methodsFor: 'private' stamp: 'AEJDS 4/28/2022 01:49:02'!
rebuildOriginalStack: aStackOfSentences with: temporalStack
	
	(temporalStack size) timesRepeat: 
		[ aStackOfSentences push: (temporalStack pop) ].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'error description' stamp: 'AEJDS 4/28/2022 00:50:19'!
emptyPrefixErrorDescription
	^ 'Prefijo a buscar no puede ser vac�o'.
	! !

!SentenceFinderByPrefix class methodsFor: 'error description' stamp: 'AEJDS 4/28/2022 00:58:36'!
prefixContainsSpaceErrorDescription
	^'Prefijo no puede contener espacios'
	! !
