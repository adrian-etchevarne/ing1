!classDefinition: #MarsRoverTest category: 'MarsRover'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverTest methodsFor: 'assertions' stamp: 'AE-JDS 5/15/2022 13:54:41'!
assertRoverAt: position headingTo: aRoverHeading process: aCommandSequence thenIsAt: anExpectedPosition headingTo: anExpectedHeading   

	| marsRover |
	
	marsRover _ MarsRover at: position heading: aRoverHeading.
	marsRover process: aCommandSequence.
	
	self assert: (marsRover isAt: anExpectedPosition heading: anExpectedHeading ).
! !


!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:01:30'!
test01RoverStaysAtOriginWhenRecievesAnEmptyStringHeadingSouth

	self 
		assertRoverAt: self defaultPosition 
		headingTo: self south 
		process: '' 
		thenIsAt: self defaultPosition 
		headingTo: self south.

! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:01:12'!
test02RoverHeadingSouthRotatesLeftThenHeadsToEast

	self 
		assertRoverAt: self defaultPosition 
		headingTo: self south
		process: 'l' 
		thenIsAt: self defaultPosition 
		headingTo: self east.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 13:59:32'!
test03RoverHeadingSouthRotatesRightThenHeadsToWest

	self 
		assertRoverAt: self defaultPosition 
		headingTo: self south 
		process: 'r' 
		thenIsAt: self defaultPosition 
		headingTo: self west.! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:02:33'!
test04RoverHeadingNorthRotatesLeftThenHeadsToWest

	self 
		assertRoverAt: self defaultPosition 
		headingTo: self north 
		process: 'l' 
		thenIsAt: self defaultPosition 
		headingTo: self west.	! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:04:20'!
test05RoverHeadingNorthRotatesRightThenHeadsToEast

		self 
		assertRoverAt: self defaultPosition 
		headingTo: self north 
		process: 'r' 
		thenIsAt: self defaultPosition 
		headingTo: self east.	! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:05:03'!
test06RoverHeadingEastRotatesLeftThenHeadsToNorth

	self 
		assertRoverAt: self defaultPosition 
		headingTo: self east 
		process: 'l' 
		thenIsAt: self defaultPosition 
		headingTo: self north.
		! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:05:43'!
test07RoverHeadingEastRotatesRightThenHeadsToSouth

	self 
		assertRoverAt: self defaultPosition 
		headingTo: self east 
		process: 'r' 
		thenIsAt: self defaultPosition 
		headingTo: self south.! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:06:12'!
test08RoverHeadingWestRotatesLeftThenHeadsToSouth

	self 
		assertRoverAt: self defaultPosition 
		headingTo: self west 
		process: 'l' 
		thenIsAt: self defaultPosition 
		headingTo: self south.! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:06:46'!
test09RoverHeadingWestRotatesRightThenHeadsToNorth

	self 
		assertRoverAt: self defaultPosition 
		headingTo: self west 
		process: 'r' 
		thenIsAt: self defaultPosition 
		headingTo: self north.! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:07:46'!
test10RoverHeadingSouthCanProcessMultipleRotateCommands

	self
		assertRoverAt: self defaultPosition 
		headingTo: self south 
		process: 'lllr' 
		thenIsAt: self defaultPosition 
		headingTo: self north.
	! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:09:42'!
test11RoverHeadingSouthMovesForwardThenDecrementsByOneItsPositionOnYAxis

	self 
		assertRoverAt: 0@0
		headingTo: self south 
		process: 'f' 
		thenIsAt: 0@-1 
		headingTo: self south.! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:10:24'!
test12RoverHeadingSouthMovesBackwardThenIncrementsByOneItsPositionOnYAxis

	self 
		assertRoverAt: 0@0
		headingTo: self south 
		process: 'b' 
		thenIsAt: 0@1 
		headingTo: self south.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:11:54'!
test13RoverHeadingSouthMovesForwardTwiceThenDecrementsByTwoItsPositionOnYAxis

	self 
		assertRoverAt: 0@0
		headingTo: self south 
		process: 'ff' 
		thenIsAt: 0@-2 
		headingTo: self south.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:13:02'!
test14RoverHeadingSouthMovesBackwardTwiceThenIncrementsByTwoItsPositionOnYAxis

	self 
		assertRoverAt: 0@0
		headingTo: self south 
		process: 'bb' 
		thenIsAt: 0@2 
		headingTo: self south.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:13:45'!
test15RoverHeadingEastMovesForwardThenIncrementsByOneItsPositionOnXAxis

	self 
		assertRoverAt: 0@0
		headingTo: self east 
		process: 'f' 
		thenIsAt: 1@0 
		headingTo: self east.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:14:33'!
test16RoverHeadingEastMovesBackwardThenDecrementsByOneItsPositionOnXAxis

	self 
		assertRoverAt: 0@0
		headingTo: self east 
		process: 'b' 
		thenIsAt: -1@0 
		headingTo: self east.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:15:13'!
test17RoverHeadingEastMovesForwardTwiceThenIncrementsByTwoItsPositionOnXAxis

	self 
		assertRoverAt: 0@0
		headingTo: self east 
		process: 'ff' 
		thenIsAt: 2@0 
		headingTo: self east.! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:15:44'!
test18RoverHeadingEastMovesBackwardTwiceThenDecrementsByTwoItsPositionOnXAxis

	self 
		assertRoverAt: 0@0
		headingTo: self east 
		process: 'bb' 
		thenIsAt: -2@0 
		headingTo: self east.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:16:53'!
test19RoverHeadingNorthMovesForwardThenIncrementsByOneItsPositionOnYAxis

	self 
		assertRoverAt: 0@0
		headingTo: self north 
		process: 'f' 
		thenIsAt: 0@1 
		headingTo: self north.! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:18:10'!
test20RoverHeadingNorthMovesForwardTwiceThenIncrementsByTwoItsPositionOnYAxis

	self 
		assertRoverAt: 0@0
		headingTo: self north 
		process: 'ff' 
		thenIsAt: 0@2 
		headingTo: self north.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:18:45'!
test21RoverHeadingNorthMovesBackwardThenDecrementsByOneItsPositionOnYAxis

	self 
		assertRoverAt: 0@0
		headingTo: self north 
		process: 'b' 
		thenIsAt: 0@-1 
		headingTo: self north.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:19:17'!
test22RoverHeadingNorthMovesBackwardTwiceThenDecrementsByTwoItsPositionOnYAxis

	self 
		assertRoverAt: 0@0
		headingTo: self north 
		process: 'bb' 
		thenIsAt: 0@-2 
		headingTo: self north.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:19:54'!
test23RoverHeadingWestMovesForwardThenDecrementsByOneItsPositionOnXAxis

	self 
		assertRoverAt: 0@0
		headingTo: self west 
		process: 'f' 
		thenIsAt: -1@0 
		headingTo: self west.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:20:32'!
test24RoverHeadingWestMovesForwardTwiceThenDecrementsByTwoItsPositionOnXAxis

	self 
		assertRoverAt: 0@0
		headingTo: self west 
		process: 'ff' 
		thenIsAt: -2@0 
		headingTo: self west.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:21:01'!
test25RoverHeadingWestMovesBackwardThenIncrementsByOneItsPositionOnXAxis

	self 
		assertRoverAt: 0@0
		headingTo: self west 
		process: 'b' 
		thenIsAt: 1@0 
		headingTo: self west.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:21:32'!
test26RoverHeadingWestMovesBackwardTwiceThenIncrementsByTwoItsPositionOnXAxis

	self 
		assertRoverAt: 0@0
		headingTo: self west 
		process: 'bb' 
		thenIsAt: 2@0 
		headingTo: self west.
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 14:33:59'!
test27RoverReceivesABadCommandThenDoesntProcessTheRestingCommands

	| marsRover |

	marsRover _ MarsRover at: 0@0 heading: self west.
	
	self  should: [marsRover process: 'rfXl'] 
		raise: Error - MessageNotUnderstood 
		withMessageText: MarsRover commandNotRecognizedErrorDescription. 
	
	self assert: (marsRover isAt: 0@1 heading: self north).! !


!MarsRoverTest methodsFor: 'setup' stamp: 'AE-JDS 5/15/2022 13:58:12'!
defaultPosition

	^ 0@0! !

!MarsRoverTest methodsFor: 'setup' stamp: 'AE-JDS 5/15/2022 13:57:52'!
east
	
	^ RoverHeadingEast! !

!MarsRoverTest methodsFor: 'setup' stamp: 'AE-JDS 5/15/2022 14:02:54'!
north
	
	^ RoverHeadingNorth ! !

!MarsRoverTest methodsFor: 'setup' stamp: 'AE-JDS 5/15/2022 13:42:54'!
south

	^ RoverHeadingSouth! !

!MarsRoverTest methodsFor: 'setup' stamp: 'AE-JDS 5/15/2022 14:00:18'!
west
	
	^ RoverHeadingWest! !


!classDefinition: #MarsRover category: 'MarsRover'!
Object subclass: #MarsRover
	instanceVariableNames: 'position heading'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRover methodsFor: 'initialization' stamp: 'AE-JDS 5/15/2022 04:06:37'!
initializeAt: aPoint heading: aRoverHeading 
	
	position := aPoint.
	self headTo: aRoverHeading.
! !


!MarsRover methodsFor: 'processing' stamp: 'AE-JDS 5/15/2022 14:49:18'!
process: aCommandSequence 
	
	aCommandSequence do: [ :aCommand | self processCommand: aCommand ].
! !

!MarsRover methodsFor: 'processing' stamp: 'AE-JDS 5/15/2022 14:48:14'!
processCommand: aRoverCommand

	(self isRotateLeft: aRoverCommand) ifTrue: [ ^ heading rotateLeft ].
	(self isRotateRight: aRoverCommand) ifTrue: [ ^ heading rotateRight ].

	(self isMoveForward: aRoverCommand) ifTrue: [ ^ heading moveForward ].
	(self isMoveBackward: aRoverCommand) ifTrue: [ ^ heading moveBackward ].
	
	self signalCommandNotRecognizedError.

	! !


!MarsRover methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 12:37:54'!
isAt: aPosition heading: aHeading 
	
	^ position = aPosition and: [	heading isHeadingTo: aHeading].! !


!MarsRover methodsFor: 'private-accessing' stamp: 'AE-JDS 5/15/2022 04:05:31'!
headTo: aRoverHeading

	heading _ aRoverHeading for: self.! !

!MarsRover methodsFor: 'private-accessing' stamp: 'AE-JDS 5/15/2022 14:54:09'!
moveEast
	
	position _ 1@0 + position.! !

!MarsRover methodsFor: 'private-accessing' stamp: 'AE-JDS 5/15/2022 14:52:16'!
moveNorth
	
	position _ 0@1 + position.! !

!MarsRover methodsFor: 'private-accessing' stamp: 'AE-JDS 5/15/2022 14:51:07'!
moveSouth
	
	position _ 0@-1 + position.! !

!MarsRover methodsFor: 'private-accessing' stamp: 'AE-JDS 5/15/2022 14:54:37'!
moveWest
	
	position _ -1@0 + position.! !


!MarsRover methodsFor: 'private-testing' stamp: 'AE-JDS 5/15/2022 14:28:10'!
isMoveBackward: aCommand

	^ aCommand = $b! !

!MarsRover methodsFor: 'private-testing' stamp: 'AE-JDS 5/15/2022 14:27:37'!
isMoveForward: aCommand

	^ aCommand = $f! !

!MarsRover methodsFor: 'private-testing' stamp: 'AE-JDS 5/15/2022 14:26:49'!
isRotateLeft: aCommand

	^ aCommand = $l! !

!MarsRover methodsFor: 'private-testing' stamp: 'AE-JDS 5/15/2022 14:27:15'!
isRotateRight: aCommand

	^ aCommand = $r! !


!MarsRover methodsFor: 'signaling' stamp: 'AE-JDS 5/15/2022 14:33:19'!
signalCommandNotRecognizedError
	
	self error: self class commandNotRecognizedErrorDescription.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover'!
MarsRover class
	instanceVariableNames: ''!

!MarsRover class methodsFor: 'instance creation' stamp: 'AE-JDS 5/15/2022 04:05:20'!
at: aPoint heading: aRoverHeading 
	
	^self new initializeAt: aPoint heading: aRoverHeading ! !


!MarsRover class methodsFor: 'error description' stamp: 'AE-JDS 5/15/2022 14:33:36'!
commandNotRecognizedErrorDescription

	^ 'Command not recognized.'! !


!classDefinition: #RoverHeading category: 'MarsRover'!
Object subclass: #RoverHeading
	instanceVariableNames: 'rover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!RoverHeading methodsFor: 'initialization' stamp: 'AE-JDS 5/15/2022 04:00:16'!
initializeFor: aMarsRover

	rover _ aMarsRover.! !


!RoverHeading methodsFor: 'testing' stamp: 'AE-JDS 5/15/2022 12:39:50'!
isHeadingTo: aRoverHeading 
	
	^ self class = aRoverHeading ! !


!RoverHeading methodsFor: 'private-orientation' stamp: 'AE-JDS 5/15/2022 14:37:59'!
rotateLeft

	self subclassResponsibility ! !

!RoverHeading methodsFor: 'private-orientation' stamp: 'AE-JDS 5/15/2022 14:37:52'!
rotateRight

	self subclassResponsibility ! !


!RoverHeading methodsFor: 'private-position' stamp: 'AE-JDS 5/15/2022 14:38:54'!
moveBackward

	self subclassResponsibility ! !

!RoverHeading methodsFor: 'private-position' stamp: 'AE-JDS 5/15/2022 14:38:49'!
moveForward

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RoverHeading class' category: 'MarsRover'!
RoverHeading class
	instanceVariableNames: ''!

!RoverHeading class methodsFor: 'instance creation' stamp: 'AE-JDS 5/15/2022 03:59:47'!
for: aMarsRover

	^ self new initializeFor: aMarsRover! !


!classDefinition: #RoverHeadingEast category: 'MarsRover'!
RoverHeading subclass: #RoverHeadingEast
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!RoverHeadingEast methodsFor: 'private-orientation' stamp: 'AE-JDS 5/15/2022 14:37:18'!
rotateLeft

	rover headTo: RoverHeadingNorth.! !

!RoverHeadingEast methodsFor: 'private-orientation' stamp: 'AE-JDS 5/15/2022 14:37:32'!
rotateRight

	rover headTo: RoverHeadingSouth.! !


!RoverHeadingEast methodsFor: 'private-position' stamp: 'AE-JDS 5/15/2022 14:40:26'!
moveBackward

	rover moveWest.! !

!RoverHeadingEast methodsFor: 'private-position' stamp: 'AE-JDS 5/15/2022 14:39:54'!
moveForward

	rover moveEast.! !


!classDefinition: #RoverHeadingNorth category: 'MarsRover'!
RoverHeading subclass: #RoverHeadingNorth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!RoverHeadingNorth methodsFor: 'private-orientation' stamp: 'AE-JDS 5/15/2022 14:41:30'!
rotateLeft

	rover headTo: RoverHeadingWest.! !

!RoverHeadingNorth methodsFor: 'private-orientation' stamp: 'AE-JDS 5/15/2022 14:41:40'!
rotateRight

	rover headTo: RoverHeadingEast.! !


!RoverHeadingNorth methodsFor: 'private-position' stamp: 'AE-JDS 5/15/2022 14:43:34'!
moveBackward

	rover moveSouth.! !

!RoverHeadingNorth methodsFor: 'private-position' stamp: 'AE-JDS 5/15/2022 14:43:17'!
moveForward

	rover moveNorth.! !


!classDefinition: #RoverHeadingSouth category: 'MarsRover'!
RoverHeading subclass: #RoverHeadingSouth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!RoverHeadingSouth methodsFor: 'private-orientation' stamp: 'AE-JDS 5/15/2022 14:44:57'!
rotateLeft

	rover headTo: RoverHeadingEast.! !

!RoverHeadingSouth methodsFor: 'private-orientation' stamp: 'AE-JDS 5/15/2022 14:45:08'!
rotateRight

	rover headTo: RoverHeadingWest.! !


!RoverHeadingSouth methodsFor: 'private-position' stamp: 'AE-JDS 5/15/2022 14:45:39'!
moveBackward

	rover moveNorth.! !

!RoverHeadingSouth methodsFor: 'private-position' stamp: 'AE-JDS 5/15/2022 14:45:27'!
moveForward

	rover moveSouth.! !


!classDefinition: #RoverHeadingWest category: 'MarsRover'!
RoverHeading subclass: #RoverHeadingWest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!RoverHeadingWest methodsFor: 'private-orientation' stamp: 'AE-JDS 5/15/2022 14:46:18'!
rotateLeft

	rover headTo: RoverHeadingSouth.! !

!RoverHeadingWest methodsFor: 'private-orientation' stamp: 'AE-JDS 5/15/2022 14:46:25'!
rotateRight

	rover headTo: RoverHeadingNorth.! !


!RoverHeadingWest methodsFor: 'private-position' stamp: 'AE-JDS 5/15/2022 14:46:56'!
moveBackward

	rover moveEast.! !

!RoverHeadingWest methodsFor: 'private-position' stamp: 'AE-JDS 5/15/2022 14:46:46'!
moveForward

	rover moveWest.! !
