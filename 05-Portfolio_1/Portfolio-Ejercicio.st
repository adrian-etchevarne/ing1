!classDefinition: #BankInstrumentTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #BankInstrumentTest
	instanceVariableNames: 'portfolio rootPortfolio anotherPortfolio repeatedPortfolio anotherRootPortfolio childPortfolio anotherChildPortfolio repeatedAccountPortfolio acc'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!BankInstrumentTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:44'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	| account |
	
	account := ReceptiveAccount new.

	self assert: 0 equals: account balance.
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:48'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
		
	self assert: 100 equals: account balance.
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'NR 11/2/2020 17:13:52'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance.
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:32'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := 50.
	
	self assert: withdrawValue equals: (Withdraw register: withdrawValue on: account) value
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'NR 5/16/2022 16:18:39'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'NR 5/16/2022 16:19:11'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| deposit withdraw account |
	
	account := ReceptiveAccount new.
	deposit :=  Deposit for: 200.
	withdraw := Withdraw for: 50.
	
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/22/2022 17:16:45'!
test07AccountKnowsItsTransactions 

	| account deposit |
	
	account := ReceptiveAccount new.
	
	deposit := Deposit register: 50 on: account.
		
	self assert: 1 equals: account transactions size.
	self assert: (account transactions includes: deposit).
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:05:49'!
test08AnEmptyPortfolioHasZeroAsBalanceWhenIsCreated 

	self createPortfolios.
	
	self assert: 0 equals: portfolio balance.
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:06:15'!
test09OneAccountPortfolioBalanceIsEqualsToBalanceAccount 

	| account |

	self createPortfolios.

	account := self addNewAccountWithDepositOf: 100 on: portfolio.
	
	self assert: account balance equals: portfolio balance.
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:06:25'!
test10MultipleAccountsPortfolioBalanceIsEqualsToBalanceAccountsSum 

	| account anotherAccount |

	self createPortfolios.
	
	account := self addNewAccountWithDepositOf: 100 on: portfolio.
	anotherAccount := 	self addANewAccountWithDepositOf: 100 andWithdrawOf: 50 on: portfolio. 

	self assert: account balance + anotherAccount balance equals: portfolio balance.
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:06:38'!
test11PortfolioWithOneAccountKnowsItsMultipleTransactionsAccount

	| account deposit withdraw |

	self createPortfolios.

	account := ReceptiveAccount new.

	deposit _ Deposit register: 100 on: account.
	withdraw _ Withdraw register: 50 on: account. 

	portfolio addBankInstrument: account.
	
	self assert: (portfolio transactions includes: deposit). 
	self assert: (portfolio transactions includes: withdraw). 
	self assert: 2 equals: portfolio transactions size.! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:06:48'!
test12PortfolioWithMultipleAccountsKnowsItsTransactions

	| account deposit withdraw anotherAccount anotherDeposit |
	
	self createPortfolios.
	
	account := ReceptiveAccount new.
	anotherAccount := ReceptiveAccount new.

	deposit _ Deposit register: 100 on: account.
	portfolio addBankInstrument: account.
	
	anotherDeposit _ Deposit register: 100 on: anotherAccount.
	withdraw _ Withdraw register: 50 on: anotherAccount.	
	portfolio addBankInstrument: anotherAccount.
	
	self assert: (portfolio transactions includes: deposit). 
	self assert: (portfolio transactions includes: anotherDeposit). 
	self assert: (portfolio transactions includes: withdraw). 
	self assert: 3 equals: portfolio transactions size.! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:06:59'!
test13PortfolioKnowsIfAParticularTransactionIsRegistered

	| account withdraw anotherAccount deposit |
	
	self createPortfolios.

	account := ReceptiveAccount new.
	anotherAccount := ReceptiveAccount new.

	deposit _ Deposit register: 100 on: account.
	portfolio addBankInstrument: account.
	
	Deposit register: 100 on: anotherAccount.
	withdraw _ Withdraw register: 50 on: anotherAccount.	
	portfolio addBankInstrument: anotherAccount.
	
	self assert: (portfolio hasRegistered: deposit). 
	self assert: (portfolio hasRegistered: withdraw). 
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:07:13'!
test14PortfolioKnowsIfAParticularTransactionIsNotRegistered

	| account deposit |
	
	self createPortfolios.
	
	self addANewAccountWithDepositOf: 100 andWithdrawOf: 50  on: portfolio.	
	
	account := ReceptiveAccount new.
	deposit _ Deposit register: 100 on: account.
	
	self deny: (portfolio hasRegistered: deposit). 

! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:07:29'!
test15APortfolioCannotIncludesRepeatedAccountsOnTheSameTreeLevel

	|account |
	
	self createPortfolios.

	account := self addNewAccountWithDepositOf: 50 on: portfolio. 
	self addANewAccountWithDepositOf: 100 andWithdrawOf: 50 on: portfolio. 
	
	self  should: [	 portfolio addBankInstrument: account] 
		raise: Error - MessageNotUnderstood 
		withMessageText: portfolio aPortfolioCannotIncludesRepeatedABankInstrumentErrorDescription

! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:07:39'!
test16APortfolioCanIncludesAnotherPortfolio

	self createPortfolios.
	
	portfolio addBankInstrument: anotherPortfolio.
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:07:52'!
test17TheBalanceOfAPortfolioIncludingAnotherPortfolioIsTheSumOfItsIndividualBalance

	| individualAnotherPortfolioBalance individualPortfolioBalance |
	
	self createPortfolios.
	
	self addNewAccountWithDepositOf: 100 on: portfolio.
	individualPortfolioBalance _ portfolio balance.

	self addANewAccountWithDepositOf: 100 andWithdrawOf: 50 on: anotherPortfolio .
	individualAnotherPortfolioBalance _ anotherPortfolio balance.

	portfolio addBankInstrument: anotherPortfolio.

	self assert: individualPortfolioBalance + individualAnotherPortfolioBalance equals: portfolio balance.
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:07:58'!
test18APortfolioCannotIncludesItSelf

	self createPortfolios.
	
	self  should: [portfolio addBankInstrument: portfolio] 
		raise: Error - MessageNotUnderstood 
		withMessageText: portfolio aPortfolioCannotIncludesRepeatedABankInstrumentErrorDescription.
	
	
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:08:06'!
test19APortfolioCannotIncludesRepeatedPortfoliosOnTheSameTreeLevel

	"                       rootPortfolio
	                           /          \
  		    portfolio         portfolio    <--- No se puede agregar un portfolio repetido en el mismo nivel     	"
	
	self createPortfolios.
	
	rootPortfolio addBankInstrument: portfolio.
	
	self  should: [rootPortfolio addBankInstrument: portfolio] 
		raise: Error - MessageNotUnderstood 
		withMessageText: rootPortfolio aPortfolioCannotIncludesRepeatedABankInstrumentErrorDescription. 

! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:08:15'!
test20APortfolioCannotIncludesRepeatedPortfoliosOnItsDescendents

	"                         rootPortfolio
	                              /         \
	repeatedPortfolio        portfolio  <--- No se puede agregar un portfolio que contiene un portfolio repetido!!  
	                                              |
	                                repeatedPortfolio   "
	
	self createPortfolios.
	
	rootPortfolio addBankInstrument: repeatedPortfolio.
	portfolio addBankInstrument: repeatedPortfolio.
	
	self  should: [rootPortfolio addBankInstrument: portfolio] 
		raise: Error - MessageNotUnderstood 
		withMessageText: rootPortfolio aPortfolioCannotIncludesRepeatedABankInstrumentErrorDescription. ! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:08:22'!
test21APortfolioCannotIncludesRepeatedAccountsOnItsDescendents

	"                          rootPortfolio
	                              /         \
	 repeatedAccount       portfolio             <--- No se puede agregar un portfolio que contiene una cuenta repetida!!
	                                             |
	                     repeatedAccountPortfolio  
	                                             |  
	                              repeatedAccount                                        "

	|  repeatedAccount |
	
	self createPortfolios.
	
	repeatedAccount := ReceptiveAccount new.
	rootPortfolio addBankInstrument: repeatedAccount.

	repeatedAccountPortfolio addBankInstrument: repeatedAccount.

	portfolio addBankInstrument: repeatedAccountPortfolio.

	self  should: [rootPortfolio addBankInstrument: portfolio] 
		raise: Error - MessageNotUnderstood 
		withMessageText: rootPortfolio aPortfolioCannotIncludesRepeatedABankInstrumentErrorDescription.
		
		
! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:08:30'!
test22APortfolioCanIncludesARepeatedPortfolioOnItsAncestorsHavingDifferentRootsAndBalancesForEachRootAreCorrect

	"       rootPortfolio              anotherRootPortfolio      (rootPortfolio addBankInstrument: portfolio)        rootPortfolio        anotherRootPortfolio -> repeatedPortfolio
	              |                                    /                \                                              =>                                                           /          \           /           
	   repeatedPortfolio       portfolio    repeatedPortfolio                                                             repeatedPortfolio      portfolio      <---  S� se puede agregar este portfolio 
	 	    			        |			    									           			         |	                      a la ra�z porque est� en dos arboles			
                                                    account  																	             			   account              que tienen ra�ces diferentes		 "
				
	self createPortfolios.
	
	self addANewAccountWithDepositOf: 100 andWithdrawOf: 50 on: portfolio.
	
	rootPortfolio addBankInstrument: repeatedPortfolio.
	
	anotherRootPortfolio addBankInstrument: portfolio.
	anotherRootPortfolio addBankInstrument: repeatedPortfolio.
	
	rootPortfolio addBankInstrument: portfolio.
	self assert: rootPortfolio balance equals: anotherRootPortfolio balance.
	
	
	! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:08:38'!
test23APortfolioCannotIncludesRepeatedPortfoliosOnItsAncestors

	"                 rootPortfolio                             portfolio     (anohterPortfolio addBankInstrument: portfolio)          rootPortfolio               
	                  /                \                                      |                                        													=>                                                     /          \                  
	repeatedPortfolio   anotherPortfolio     repeatedPortfolio                                                           repeatedPortfolio      anotherPortfolio
																			                      |
				        									         		No se puede agregar!!   --->      portfolio         
	 																		                      |
	     																	                 repeatedPortfolio               "
	self createPortfolios.
	
	portfolio addBankInstrument: repeatedPortfolio.
	
	rootPortfolio addBankInstrument: anotherPortfolio.
	rootPortfolio addBankInstrument: repeatedPortfolio.
	
	self  should: [anotherPortfolio addBankInstrument: portfolio] 
		raise: Error - MessageNotUnderstood 
		withMessageText: portfolio aPortfolioCannotIncludesRepeatedABankInstrumentErrorDescription. ! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:08:45'!
test24APortfolioCanIncludesARepeatedAccountsOnItsAncestorsHavingDifferentRootsAndBalancesForEachRootAreCorrect

	"    rootPortfolio                      anotherRootPortfolio    (rootPortfolio addBankInstrument: anotherPortfolio)        rootPortfolio       anotherRootPortfolio
	              |                                       /                \                                                             =>                                                         /          \						        											/                                \
	   childPortfolio                portfolio    anotherChildPortfolio                                                                                childPortfolio      portfolio              anotherChildPortfolio
	              |                                                            |													                                                              |                      ^--  S� se puedeagregar  
 	 repeatedAccount                             repeatedAccount											                                   						         repeatedAccount              			porque tienen raices diferentes!!
	"
	
	| repeatedAccount |
	
	self createPortfolios.
	
	repeatedAccount := self addNewAccountWithDepositOf: 100 on: childPortfolio.
	anotherChildPortfolio addBankInstrument: repeatedAccount.

	self addANewAccountWithDepositOf: 100 andWithdrawOf: 50 on: portfolio.

	rootPortfolio addBankInstrument: childPortfolio.
	
	anotherRootPortfolio addBankInstrument: portfolio.
	anotherRootPortfolio addBankInstrument: anotherChildPortfolio.
		
	rootPortfolio addBankInstrument: portfolio. 
	self assert: rootPortfolio balance equals: anotherRootPortfolio balance.

	! !

!BankInstrumentTest methodsFor: 'tests' stamp: 'AE-JDS 5/23/2022 00:08:51'!
test25APortfolioCannotIncludesRepeatedAccountsOnItsAncestors

	"                                           rootPortfolio             (portfolio addBankInstrument: childPortfolio)                  rootPortfolio
	                                             /                \                                                                                                                  /                \
     anotherRootPortfolio    portfolio   anotherChildPortfolio                  					 =>       anotherRootPortfolio      portfolio      childRootPortfolio         
	              |                                                   |											                                               \              	/								|
	   childPortfolio                          repeatedAccount	                No se puede agregar  --->       childPortfolio       															repeatedAccount	
		    |							        porque tienen la misma ra�z!!				         		            |
	  repeatedAccount 												                            repeatedAccount                          "
	
	| repeatedAccount |
	
	self createPortfolios.
	
	repeatedAccount := ReceptiveAccount new.

	anotherRootPortfolio addBankInstrument: childPortfolio.
	childPortfolio addBankInstrument: repeatedAccount.
	
	rootPortfolio addBankInstrument: portfolio.
	rootPortfolio addBankInstrument: anotherChildPortfolio.
	anotherChildPortfolio addBankInstrument: repeatedAccount.
		
	self  should: [ portfolio addBankInstrument: childPortfolio] 
		raise: Error - MessageNotUnderstood 
		withMessageText: anotherRootPortfolio aPortfolioCannotIncludesRepeatedABankInstrumentErrorDescription.! !


!BankInstrumentTest methodsFor: 'instance creation' stamp: 'AE-JDS 5/22/2022 00:46:49'!
addANewAccountWithDepositOf: anAmountToDeposit andWithdrawOf: anAmountToWithdraw on: aPortfolio 
	
	| anotherAccount |

	anotherAccount _ self addNewAccountWithDepositOf: anAmountToDeposit on: aPortfolio.
	Withdraw register: anAmountToWithdraw on: anotherAccount.	
	^ anotherAccount! !

!BankInstrumentTest methodsFor: 'instance creation' stamp: 'AE-JDS 5/22/2022 00:46:49'!
addNewAccountWithDepositOf: anAmountToDeposit on: aPortfolio

	| account |
	account := ReceptiveAccount  new.
	Deposit register: anAmountToDeposit on: account.
	aPortfolio addBankInstrument: account.
	^ account ! !

!BankInstrumentTest methodsFor: 'instance creation' stamp: 'AE-JDS 5/23/2022 00:04:59'!
createPortfolios

	rootPortfolio := Portfolio new.
	anotherRootPortfolio := Portfolio new.
	portfolio := Portfolio new.
	anotherPortfolio _ Portfolio new.
	repeatedPortfolio _ Portfolio new.
	childPortfolio := Portfolio new.
	anotherChildPortfolio := Portfolio new.
	repeatedAccountPortfolio := Portfolio new.! !


!classDefinition: #AccountTransaction category: 'Portfolio-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountTransaction methodsFor: 'value' stamp: 'HernanWilkinson 9/12/2011 12:25'!
value 

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: 'Portfolio-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'NR 10/17/2019 03:22:00'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #Deposit category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !


!Deposit methodsFor: 'balance' stamp: 'AE-JDS 5/16/2022 20:15:26'!
affectTo: aBalance

	^ aBalance + self value 
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: 'Portfolio-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Withdraw category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Withdraw methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:46'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:33'!
value

	^ value! !


!Withdraw methodsFor: 'balance' stamp: 'AE-JDS 5/16/2022 20:15:26'!
affectTo: aBalance 
	
	^ aBalance - self value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: 'Portfolio-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #BankInstrument category: 'Portfolio-Ejercicio'!
Object subclass: #BankInstrument
	instanceVariableNames: 'parentPortfolios'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!BankInstrument methodsFor: 'initialization' stamp: 'AE-JDS 5/18/2022 19:40:49'!
initialize

	parentPortfolios _ OrderedCollection new.! !


!BankInstrument methodsFor: 'private' stamp: 'AE-JDS 5/22/2022 15:18:42'!
allMyBankInstrumentsAncestors
	
	"I'm an ancestor of myself"
	
	^ parentPortfolios inject: (Set with: self) into: [ :ac :aPortfolio | ac add: aPortfolio; addAll: aPortfolio allMyBankInstrumentsAncestors; yourself ]! !

!BankInstrument methodsFor: 'private' stamp: 'AE-JDS 5/22/2022 16:08:04'!
allMyBankInstrumentsDescendents

	self subclassResponsibility ! !

!BankInstrument methodsFor: 'private' stamp: 'AE-JDS 5/22/2022 23:51:05'!
allMyBankInstrumentsFromMyRoots
	
	^ self allMyBankInstrumentsRoots inject: Set new into: [ :ac :aRootPortfolio | ac addAll: aRootPortfolio allMyBankInstrumentsDescendents; yourself ] 

	! !

!BankInstrument methodsFor: 'private' stamp: 'AE-JDS 5/22/2022 23:52:12'!
allMyBankInstrumentsRoots
		
	^ self allMyBankInstrumentsAncestors select: [ :aBankInstrument | aBankInstrument isARootPortfolio ]! !

!BankInstrument methodsFor: 'private' stamp: 'AE-JDS 5/22/2022 23:53:03'!
isARootPortfolio
	
	^ parentPortfolios isEmpty! !

!BankInstrument methodsFor: 'private' stamp: 'AE-JDS 5/18/2022 19:42:14'!
isAddedTo: aPortfolio 
	
	parentPortfolios add: aPortfolio.! !


!classDefinition: #Portfolio category: 'Portfolio-Ejercicio'!
BankInstrument subclass: #Portfolio
	instanceVariableNames: 'bankinstruments'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Portfolio methodsFor: 'balance' stamp: 'AE-JDS 5/18/2022 18:24:54'!
balance
	
	^ bankinstruments sum: [ :aBankInstrument | aBankInstrument balance ] ifEmpty: [ 0 ].
! !


!Portfolio methodsFor: 'initialization' stamp: 'AE-JDS 5/18/2022 19:40:39'!
initialize

	"childPortfolios: portfolios included by myself
	parentPortfolios: portfolios including to me"

	super initialize.
	bankinstruments _ OrderedCollection new.
! !


!Portfolio methodsFor: 'assertions' stamp: 'AE-JDS 5/18/2022 19:19:57'!
assertPortfolioDoesNotIncludesRepeatedPortfoliosOnBothTrees: aPortfolio 
	
	^ (self includesRepeatedBankInstrumentOnBothPortfoliosTrees: aPortfolio) ifTrue: [ self signalAPortfolioCannotIncludesRepeatedBankInstrumentError ]! !


!Portfolio methodsFor: 'adding' stamp: 'AE-JDS 5/18/2022 19:47:06'!
addBankInstrument: aBankInstrument 
		
	self assertPortfolioDoesNotIncludesRepeatedPortfoliosOnBothTrees: aBankInstrument.
		
	bankinstruments add: aBankInstrument.
	aBankInstrument isAddedTo: self.! !


!Portfolio methodsFor: 'transactions' stamp: 'AE-JDS 5/18/2022 18:24:54'!
transactions
	
	^ bankinstruments 
		inject: OrderedCollection new 
		into: [ :allTransactions :aReceptiveAccount | 
				allTransactions addAll: aReceptiveAccount transactions. allTransactions ]. 
! !


!Portfolio methodsFor: 'testing' stamp: 'AE-JDS 5/18/2022 18:24:54'!
hasRegistered: aTransaction
	
	^ bankinstruments anySatisfy: [ :aReceptiveAccount | aReceptiveAccount hasRegistered: aTransaction ]! !

!Portfolio methodsFor: 'testing' stamp: 'AE-JDS 5/21/2022 23:58:00'!
includesRepeatedBankInstrumentOnBothPortfoliosTrees: aPortfolio 
	
	^ (self allMyBankInstrumentsFromMyRoots intersection: aPortfolio allMyBankInstrumentsDescendents ) isEmpty not.! !


!Portfolio methodsFor: 'error description' stamp: 'AE-JDS 5/18/2022 18:18:57'!
aPortfolioCannotIncludesRepeatedABankInstrumentErrorDescription
	
	^ 'A bank instrument can not includes repeated instruments'! !


!Portfolio methodsFor: 'signaling' stamp: 'AE-JDS 5/18/2022 18:18:02'!
signalAPortfolioCannotIncludesRepeatedBankInstrumentError
	
	self error: self aPortfolioCannotIncludesRepeatedABankInstrumentErrorDescription.! !


!Portfolio methodsFor: 'private' stamp: 'AE-JDS 5/18/2022 19:59:58'!
allMyBankInstrumentsDescendents

	"I'm a descendant of myself"
	
	^ bankinstruments inject: (Set with: self) into: [ :ac :aBankInstrument | ac add: aBankInstrument; addAll: aBankInstrument allMyBankInstrumentsDescendents; yourself ]! !


!classDefinition: #ReceptiveAccount category: 'Portfolio-Ejercicio'!
BankInstrument subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'AE-JDS 5/18/2022 19:42:58'!
initialize

	super initialize.
	transactions := OrderedCollection new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'AE-JDS 5/16/2022 21:18:59'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'balance' stamp: 'AE-JDS 5/16/2022 20:15:26'!
balance

	^ transactions inject: 0 into: [ :ac :aTransaction | aTransaction affectTo: ac ]! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'AE-JDS 5/18/2022 12:16:44'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !


!ReceptiveAccount methodsFor: 'private' stamp: 'AE-JDS 5/22/2022 16:09:34'!
allMyBankInstrumentsDescendents

	"I'm a descendant of myself"


	^ Set with: self! !
