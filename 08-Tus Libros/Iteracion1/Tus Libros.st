!classDefinition: #CartTest category: 'Tus Libros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'bookInCatalogue bookNotInCatalogue catalogue'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Tus Libros'!

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/5/2022 17:38:37'!
test01CartIsEmptyWhenCreated

	| cart |
	cart _ self aNewCart.
	
	self assert: cart isEmpty.
	! !

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/5/2022 18:50:12'!
test02AValidBookCanBeAddedToAnEmptyCartThenCartIncludesIt

	| cart |
	cart _ self aNewCart .
	
	cart add: bookInCatalogue .
	
	self assert: cart includes: bookInCatalogue .
	! !

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/5/2022 18:06:44'!
test03AnInvalidBookCannotBeAdded

	| cart  |
	cart _ self aNewCart .

	self should: [ cart add: bookNotInCatalogue. ]
		raise: Error - MessageNotUnderstood 
		withMessageText: Cart cannotAddBookNotIncludedInCatalogueErrorDescription .
	
	self assert: cart isEmpty .
	! !

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/5/2022 18:42:33'!
test04BookIsAddedThenIsListedWithTheRightQuantity

	| cart |
	cart _ self aNewCart .
	
	cart add: 2 of: bookInCatalogue .
	
	self assert: 2 equals: (cart occurrencesOf: bookInCatalogue).
	! !

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/5/2022 18:12:33'!
test05BookIsAddedWithAZeroQuantityRaiseAnError

	| cart |
	cart _ self aNewCart .
	
	self should: [cart add: 0 of: bookInCatalogue ]
		raise: Error - MessageNotUnderstood 
		withMessageText: Cart illegalQuantityWhenAddingBooksErrorDescription.
		
	self assert: 	cart isEmpty.
	
	! !


!CartTest methodsFor: 'setup' stamp: 'AEJDS 6/5/2022 17:29:19'!
aBook
	^ 'a very valid ISBN'.
	! !

!CartTest methodsFor: 'setup' stamp: 'AEJDS 6/5/2022 17:41:48'!
aNewCart

	^ Cart using: catalogue .
	! !

!CartTest methodsFor: 'setup' stamp: 'AEJDS 6/5/2022 18:06:21'!
anotherBook
	^ 'a very valid ISBN, but different to the other one'.
	! !

!CartTest methodsFor: 'setup' stamp: 'AEJDS 6/5/2022 17:23:16'!
emptyCatalogue
	^ OrderedCollection new
	! !

!CartTest methodsFor: 'setup' stamp: 'AEJDS 6/5/2022 18:07:04'!
setUp
	
	catalogue _ self emptyCatalogue .
	bookInCatalogue _ self aBook.
	
	catalogue add: bookInCatalogue.
	
	bookNotInCatalogue _ self anotherBook .
	
	
	
	! !


!classDefinition: #Cart category: 'Tus Libros'!
Object subclass: #Cart
	instanceVariableNames: 'contents catalog'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Tus Libros'!

!Cart methodsFor: 'accessing' stamp: 'AEJDS 6/5/2022 18:49:18'!
includes: aBook
	^contents includes: aBook .
	! !

!Cart methodsFor: 'accessing' stamp: 'AEJDS 6/2/2022 20:58:00'!
isEmpty
	^ contents isEmpty.
	! !

!Cart methodsFor: 'accessing' stamp: 'AEJDS 6/5/2022 18:44:06'!
occurrencesOf: aBook
	
	^contents occurrencesOf: aBook.
	! !


!Cart methodsFor: 'carting' stamp: 'AEJDS 6/5/2022 18:22:33'!
add: aBook 
	
	self assertBookIsIncludedInTheCatalague: aBook.
		
	self add: 1 of: aBook.
	
	! !

!Cart methodsFor: 'carting' stamp: 'AEJDS 6/5/2022 18:23:10'!
add: aQuantity of: aBook 
	
	self assertQuantityIsPositive: aQuantity.
	
	contents add: aBook withOccurrences: aQuantity.
	! !


!Cart methodsFor: 'initialization' stamp: 'AEJDS 6/5/2022 18:22:12'!
initializeUsing: aCatalog 

	contents _ Bag new.
	catalog := aCatalog.! !


!Cart methodsFor: 'error handling' stamp: 'AEJDS 6/5/2022 17:22:35'!
signalInvalidBookAdded

	^ self error: self class cannotAddBookNotIncludedInCatalogueErrorDescription .
	
	! !


!Cart methodsFor: 'asserting' stamp: 'AEJDS 6/5/2022 18:17:54'!
assertBookIsIncludedInTheCatalague: aBook

	^ (catalog includes: aBook)
		ifFalse: [ self signalInvalidBookAdded ]! !

!Cart methodsFor: 'asserting' stamp: 'AEJDS 6/5/2022 18:18:16'!
assertQuantityIsPositive: aQuantity 
	(aQuantity <= 0) 
		ifTrue: 
			[ self error: self class illegalQuantityWhenAddingBooksErrorDescription . ].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'Tus Libros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'error description' stamp: 'AEJDS 6/5/2022 17:13:58'!
cannotAddBookNotIncludedInCatalogueErrorDescription
	^ 'No se puede agregar un libro que no est� en el cat�logo'.
	! !

!Cart class methodsFor: 'error description' stamp: 'AEJDS 6/5/2022 18:14:05'!
illegalQuantityWhenAddingBooksErrorDescription
	
	^ 'Cantidad a agregar debe ser mayor a cero'.! !


!Cart class methodsFor: 'instance creation' stamp: 'AEJDS 6/2/2022 21:11:01'!
using: aCatalog 

	^self new initializeUsing: aCatalog ! !


!Cart class methodsFor: 'error handling' stamp: 'AEJDS 6/5/2022 17:22:06'!
signalInvalidBookAdded
	self shouldBeImplemented.! !
