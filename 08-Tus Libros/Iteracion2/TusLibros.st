!classDefinition: #MPTransactionError category: 'TusLibros'!
Error subclass: #MPTransactionError
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!


!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'testObjects'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/9/2022 14:56:46'!
test01NewCartsAreCreatedEmpty

	self assert: testObjects createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/9/2022 15:39:14'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := testObjects createCart.
	
	self 
		should: [ cart add: testObjects itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/9/2022 15:37:46'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := testObjects createCart.
	
	cart add: testObjects itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/9/2022 15:37:50'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := testObjects createCart.
	
	self 
		should: [cart add: 0 of: testObjects itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/9/2022 15:39:18'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := testObjects createCart.
	
	self 
		should: [cart add: 2 of: testObjects itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/9/2022 15:38:03'!
test06CartRemembersAddedItems

	| cart |
	
	cart := testObjects createCart.
	
	cart add: testObjects itemSellByTheStore.
	self assert: (cart includes: testObjects itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/9/2022 15:38:07'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := testObjects createCart.
	
	self deny: (cart includes: testObjects itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/9/2022 15:38:11'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := testObjects createCart.
	
	cart add: 2 of: testObjects itemSellByTheStore.
	
	self assert: (cart occurrencesOf: testObjects itemSellByTheStore) = 2! !


!CartTest methodsFor: 'setUp/tearDown' stamp: 'AEJDS 6/9/2022 01:32:48'!
setUp

	testObjects _ TestObjects new.
	! !


!classDefinition: #CashierTest category: 'TusLibros'!
TestCase subclass: #CashierTest
	instanceVariableNames: 'testObjects'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'setUp/tearDown' stamp: 'AEJDS 6/9/2022 01:35:36'!
setUp
	testObjects _ TestObjects new.
	! !


!CashierTest methodsFor: 'testing' stamp: 'AEJDS 6/9/2022 15:38:22'!
test01CheckOutAnEmptyCartRaiseAnException

	| cart cashier todayMonthYear merchantProcessor creditCard salesBook |
	cart _ testObjects createCart.
	cashier _ Cashier new.
	
	creditCard _ testObjects createNotExpiredCreditCard .
	
	todayMonthYear _ testObjects currentMonthYear.
	merchantProcessor _ testObjects rejectingMerchantProcessor.
	salesBook _ testObjects createSalesBook.
	
	self should: [ cashier 
				checkout: cart 
				with: creditCard
				onDate: todayMonthYear processing: merchantProcessor 
				registersOn: salesBook ]
		raise: Error - MessageNotUnderstood 
		withMessageText: cashier cannotCheckoutEmptyCartErrorDescription.
		
	self assert: salesBook isEmpty.
	
! !

!CashierTest methodsFor: 'testing' stamp: 'AEJDS 6/9/2022 15:40:27'!
test02CheckOutWithAnExpiredCreditCardRaisesException

	| cart cashier creditCard todayMonthYear merchantProcessor salesBook |
	cart _ testObjects createCart.
	cashier _ Cashier new.
	
	cart add: testObjects itemSellByTheStore .
	
	creditCard _ testObjects createExpiredCreditCard . 
	todayMonthYear _ testObjects currentMonthYear .
	merchantProcessor _ testObjects rejectingMerchantProcessor.
	salesBook _ testObjects createSalesBook.
	
	self should: [  cashier
				checkout: cart 
				with: creditCard
				onDate: todayMonthYear
				processing: merchantProcessor
				registersOn: salesBook ]
		raise: Error - MessageNotUnderstood 
		withMessageText: cashier creditCardExpiredErrorDescription.
		
	
	self assert: salesBook isEmpty.
	
		
! !

!CashierTest methodsFor: 'testing' stamp: 'AEJDS 6/9/2022 15:38:34'!
test03MerchantProcessorGivesErrorOnCheckoutRaisesAnException

	| cart cashier todayMonthYear merchantProcessor creditCard salesBook |
	cart _ testObjects createCart.
	cashier _ Cashier new.
	
	cart add: testObjects itemSellByTheStore .
	
	creditCard _ testObjects createNotExpiredCreditCard .
	todayMonthYear _ testObjects currentMonthYear .
	merchantProcessor _ testObjects rejectingMerchantProcessor.
	salesBook _ testObjects createSalesBook.
	
	self should: [ cashier
				checkout: cart 
				with: creditCard
				onDate: todayMonthYear
				processing: merchantProcessor 
				registersOn: salesBook ]
		raise: MPTransactionError.
		
	
	self assert: salesBook isEmpty.
	
		
! !

!CashierTest methodsFor: 'testing' stamp: 'AEJDS 6/9/2022 15:41:17'!
test04CheckoutWithValidCartAndMerchantProcessorAcceptsThenSaleIsRegistered

	| cart cashier todayMonthYear merchantProcessor creditCard firstSale salesBook |
	cart _ testObjects createCart.
	cashier _ Cashier new.
	
	cart add: 3 of: testObjects itemSellByTheStore .
	
	creditCard _ testObjects createNotExpiredCreditCard .
	todayMonthYear _ testObjects currentMonthYear .
	merchantProcessor _ testObjects acceptingMerchantProcessor.
	salesBook _ testObjects createSalesBook.
	
	cashier
		checkout: cart 
		with: creditCard
		onDate: todayMonthYear
		processing: merchantProcessor 
		registersOn: salesBook.
		

	firstSale _ salesBook first.
	
	self assert: (self itemPriceSellByTheStore * 3) equals: firstSale .
	
	
	
		
! !


!classDefinition: #CreditCardTest category: 'TusLibros'!
TestCase subclass: #CreditCardTest
	instanceVariableNames: 'testObjects'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCardTest methodsFor: 'tests' stamp: 'AEJDS 6/9/2022 05:19:19'!
test01NameCannotBeEmpty

	| expiresOn number owner |
	owner _ ''.
	number _ '0123456789012345	'.
	expiresOn _ FixedGregorianDate today monthOfYear .
	
	self should: [ | cc | cc _ CreditCard owner: owner number: number expiresOn: expiresOn ]
		raise: Error - MessageNotUnderstood 
		withMessageText: CreditCard ownerNameCannotBeEmptyErrorDescription.
		
		
	
	! !

!CreditCardTest methodsFor: 'tests' stamp: 'AEJDS 6/9/2022 15:59:02'!
test02CannotCreateACreditCardWithAnInvalidNumber

	| invalidCreditCardNumber owner expirationDate |
	owner _ testObjects creditCardOwner .
	invalidCreditCardNumber _ 'asereje'.
	expirationDate _ testObjects currentMonthYear next.
	
	self should: [ | cc | cc _ CreditCard owner: owner number: invalidCreditCardNumber expiresOn: expirationDate ]
		raise: Error - MessageNotUnderstood 
		withMessageText: CreditCard invalidNumberFormatErrorDescription.
		
		
	
	! !

!CreditCardTest methodsFor: 'tests' stamp: 'AEJDS 6/9/2022 15:59:27'!
test03CreditCardWithExpirationDateInThePastIsExpired

	| owner creditCard creditCardNumber dateInThePast |

	owner _ testObjects creditCardOwner .
	creditCardNumber _ testObjects creditCardNumber .
	dateInThePast _ testObjects currentMonthYear previous.
	
	creditCard _ CreditCard owner: owner number: creditCardNumber expiresOn: dateInThePast.

	
	self assert: (creditCard isExpiredOn: testObjects currentMonthYear ).
	
		
	
! !

!CreditCardTest methodsFor: 'tests' stamp: 'AEJDS 6/9/2022 16:00:28'!
test04CreditCardWithExpirationDateInTheFutureIsNotExpired

	| owner creditCard creditCardNumber dateInTheFuture |
	
	owner _ testObjects creditCardOwner .
	creditCardNumber _ testObjects creditCardNumber .
	dateInTheFuture _ testObjects currentMonthYear next .
	
	creditCard _ CreditCard owner: owner number: creditCardNumber expiresOn: dateInTheFuture.
	
	self deny: (creditCard isExpiredOn: testObjects currentMonthYear ).
	
		
	
! !


!CreditCardTest methodsFor: 'setUp/tearDown' stamp: 'AEJDS 6/9/2022 15:42:12'!
setUp
	testObjects _ TestObjects new.
	! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'AEJDS 6/9/2022 06:02:21'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !

!Cart methodsFor: 'queries' stamp: 'AEJDS 6/9/2022 15:46:04'!
total
	
	^ items sum: 
		[ :item | 
			catalog at: item ifAbsent: [ self shouldNotHappenBecause: 'Solo debieran agregarse �tems que estuvieran en el cat�logo.' ] ] 
		ifEmpty: 0 * peso.
	
	
	! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'error handling' stamp: 'AEJDS 6/6/2022 19:43:57'!
cannotCheckoutEmptyCartErrorDescription
	^ 'Cannot checkout an empty cart'.! !


!Cashier methodsFor: 'checkout' stamp: 'AEJDS 6/9/2022 04:09:36'!
assertCartIsNotEmpty: aCart

	^ aCart isEmpty ifTrue: [ self error: self cannotCheckoutEmptyCartErrorDescription ]! !

!Cashier methodsFor: 'checkout' stamp: 'AEJDS 6/9/2022 05:28:59'!
assertCreditCardNotExpired: aCreditCard on: checkDate 

	^ (aCreditCard isExpiredOn: checkDate)
		ifTrue: [ 	self error: self creditCardExpiredErrorDescription. ]! !

!Cashier methodsFor: 'checkout' stamp: 'AEJDS 6/9/2022 05:54:01'!
checkout: aCart with: aCreditCard onDate: checkDate processing: merchantProcessor registersOn: aSalesBook    

	| total |
	self assertCartIsNotEmpty: aCart.
	
	self assertCreditCardNotExpired: aCreditCard on: checkDate.
	
	total _ aCart total.
	
	merchantProcessor debitFrom: aCreditCard number anAmountOf: total.
	
	aSalesBook add: total.
	! !

!Cashier methodsFor: 'checkout' stamp: 'AEJDS 6/9/2022 03:13:55'!
creditCardExpiredErrorDescription
	^ 'Cannot checkout, credit card exired.'.
	! !



!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'owner number expirationMonthYear'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'initialization' stamp: 'AEJDS 6/9/2022 05:12:57'!
initializeOwner: aName number: aCreditCardNumber expiresOn: anExpirationMonthYear

	owner := aName.
	number _ aCreditCardNumber .
	expirationMonthYear _ anExpirationMonthYear .
	! !


!CreditCard methodsFor: 'accessing' stamp: 'AEJDS 6/9/2022 05:14:14'!
isExpiredOn: aGregorianDate
	
	^ expirationMonthYear < aGregorianDate ! !

!CreditCard methodsFor: 'accessing' stamp: 'AEJDS 6/9/2022 05:29:22'!
number

	^number! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'error handling' stamp: 'AEJDS 6/9/2022 04:56:01'!
invalidNumberFormatErrorDescription
	^ 'The format of the credit card number is incorrect'.
	! !

!CreditCard class methodsFor: 'error handling' stamp: 'AEJDS 6/9/2022 04:49:37'!
ownerNameCannotBeEmptyErrorDescription
	^'Owner name cannot be empty'.
	! !


!CreditCard class methodsFor: 'assertions' stamp: 'AEJDS 6/9/2022 16:02:20'!
assertCreditCardNumberFormatIsCorrect: aPotentialCreditCardNumber

	^ (aPotentialCreditCardNumber size = 16 and: [ aPotentialCreditCardNumber allSatisfy: [ :char | char isDigit ]. ])
		ifFalse: [	 self error: self invalidNumberFormatErrorDescription . ]! !

!CreditCard class methodsFor: 'assertions' stamp: 'AEJDS 6/9/2022 04:51:38'!
assertNameIsNotEmpty: aName

	^ (aName isEmpty) 
		ifTrue: [ self error: self ownerNameCannotBeEmptyErrorDescription ]! !


!CreditCard class methodsFor: 'instance creation' stamp: 'AEJDS 6/9/2022 16:02:23'!
owner: aName number: aPotentialCreditCardNumber expiresOn: expirationMonthDay  

	self assertNameIsNotEmpty: aName.
		
	self assertCreditCardNumberFormatIsCorrect: aPotentialCreditCardNumber.
	
	^self new initializeOwner: aName number: aPotentialCreditCardNumber expiresOn: expirationMonthDay .
	! !


!classDefinition: #FakeMerchantProcessor category: 'TusLibros'!
Object subclass: #FakeMerchantProcessor
	instanceVariableNames: 'blockClosure'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!FakeMerchantProcessor methodsFor: 'processing' stamp: 'AEJDS 6/9/2022 15:51:13'!
debitFrom: aCreditCard anAmountOf: anAmount 

	^ blockClosure value: aCreditCard value: anAmount .
	! !


!FakeMerchantProcessor methodsFor: 'initialization' stamp: 'AEJDS 6/9/2022 15:50:38'!
initializeOnDebitDo: aBlockClosure 

	blockClosure := aBlockClosure.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FakeMerchantProcessor class' category: 'TusLibros'!
FakeMerchantProcessor class
	instanceVariableNames: ''!

!FakeMerchantProcessor class methodsFor: 'instance creation' stamp: 'AEJDS 6/9/2022 15:50:18'!
onDebitDo: aBlockClosure 
	
	^self new initializeOnDebitDo: aBlockClosure ! !


!classDefinition: #TestObjects category: 'TusLibros'!
Object subclass: #TestObjects
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TestObjects methodsFor: 'objects for testing' stamp: 'AEJDS 6/9/2022 15:49:51'!
acceptingMerchantProcessor

	^ FakeMerchantProcessor onDebitDo:
		[ :creditCard :amount |  ].
		
	! !

!TestObjects methodsFor: 'objects for testing' stamp: 'AEJDS 6/9/2022 01:31:02'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog
	! !

!TestObjects methodsFor: 'objects for testing' stamp: 'AEJDS 6/9/2022 05:33:39'!
createExpiredCreditCard
	
	
	^ CreditCard 
		owner: 'Roberto' 
		number: (self creditCardNumber )
		expiresOn: (self currentMonthYear previous ).
	! !

!TestObjects methodsFor: 'objects for testing' stamp: 'AEJDS 6/9/2022 05:33:13'!
createNotExpiredCreditCard


	^CreditCard 
		owner: 'Roberto' 
		number: (self creditCardNumber) 
		expiresOn: (self currentMonthYear next).
	! !

!TestObjects methodsFor: 'objects for testing' stamp: 'AEJDS 6/9/2022 05:51:33'!
createSalesBook
	^OrderedCollection new.
	! !

!TestObjects methodsFor: 'objects for testing' stamp: 'AEJDS 6/9/2022 05:32:50'!
creditCardNumber
	^ '0123456789012345'.
	! !

!TestObjects methodsFor: 'objects for testing' stamp: 'AEJDS 6/9/2022 15:55:35'!
creditCardOwner
	^ 'Roberto'.
	! !

!TestObjects methodsFor: 'objects for testing' stamp: 'AEJDS 6/9/2022 03:11:47'!
currentMonthYear
	
	^ FixedGregorianDate today monthOfYear .
	
	! !

!TestObjects methodsFor: 'objects for testing' stamp: 'AEJDS 6/9/2022 06:01:36'!
defaultCatalog
	
	^ Dictionary new add: (self itemSellByTheStore) -> (self itemPriceSellByTheStore); yourself.
	
! !

!TestObjects methodsFor: 'objects for testing' stamp: 'AEJDS 6/9/2022 01:33:48'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!TestObjects methodsFor: 'objects for testing' stamp: 'AEJDS 6/9/2022 05:59:24'!
itemPriceSellByTheStore
	^ 3000 * peso.
	! !

!TestObjects methodsFor: 'objects for testing' stamp: 'AEJDS 6/9/2022 01:31:28'!
itemSellByTheStore
	
	^ 'validBook'! !

!TestObjects methodsFor: 'objects for testing' stamp: 'AEJDS 6/9/2022 16:11:22'!
rejectingMerchantProcessor
	
	^ FakeMerchantProcessor onDebitDo:
		[ :creditCard :amount |  MPTransactionError new signal: 'Some reason, for example: not enough funds.' ].
		
		
	! !
