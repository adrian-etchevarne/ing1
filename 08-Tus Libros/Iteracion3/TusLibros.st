!classDefinition: #MerchantProcessorError category: 'TusLibros'!
Error subclass: #MerchantProcessorError
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!


!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'testObjectsFactory'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test01NewCartsAreCreatedEmpty

	self assert: testObjectsFactory createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/12/2022 19:55:20'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [ cart add: testObjectsFactory itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart class invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:10'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 0 of: testObjectsFactory itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'AEJDS 6/12/2022 19:55:02'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self 
		should: [cart add: 2 of: testObjectsFactory itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart class invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test06CartRemembersAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: testObjectsFactory itemSellByTheStore.
	self assert: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	self deny: (cart includes: testObjectsFactory itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:11'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := testObjectsFactory createCart.
	
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	self assert: (cart occurrencesOf: testObjectsFactory itemSellByTheStore) = 2! !


!CartTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 18:09'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.! !


!classDefinition: #CashierTest category: 'TusLibros'!
TestCase subclass: #CashierTest
	instanceVariableNames: 'testObjectsFactory debitBehavior'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:50'!
test01CanNotCheckoutAnEmptyCart

	| salesBook |
	
	salesBook := OrderedCollection new.
	self 
		should: [ Cashier 
			toCheckout: testObjectsFactory createCart 
			charging: testObjectsFactory notExpiredCreditCard 
			throught: self
			on: testObjectsFactory today
			registeringOn:  salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier cartCanNotBeEmptyErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:51'!
test02CalculatedTotalIsCorrect

	| cart cashier |
	
	cart := testObjectsFactory createCart.
	cart add: 2 of: testObjectsFactory itemSellByTheStore.
	
	cashier :=  Cashier
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard 
		throught: self
		on: testObjectsFactory today 
		registeringOn: OrderedCollection new.
		
	self assert: cashier checkOut = (testObjectsFactory itemSellByTheStorePrice * 2)! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:51'!
test03CanNotCheckoutWithAnExpiredCreditCart

	| cart salesBook |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	salesBook := OrderedCollection new.
	
	self
		should: [ Cashier 
				toCheckout: cart 
				charging: testObjectsFactory expiredCreditCard 
				throught: self
				on: testObjectsFactory today
				registeringOn: salesBook ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError | 
			self assert: anError messageText = Cashier canNotChargeAnExpiredCreditCardErrorMessage.
			self assert: salesBook isEmpty ]! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 19:04'!
test04CheckoutRegistersASale

	| cart cashier salesBook total |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: testObjectsFactory notExpiredCreditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	total := cashier checkOut.
					
	self assert: salesBook size = 1.
	self assert: salesBook first total = total.! !

!CashierTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 07:22:11'!
test05CashierChargesCreditCardUsingMerchantProcessor

	| cart cashier salesBook total creditCard debitedAmout debitedCreditCard  |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	debitBehavior := [ :anAmount :aCreditCard | 
		debitedAmout := anAmount.
		debitedCreditCard := aCreditCard ].
	total := cashier checkOut.
					
	self assert: debitedCreditCard = creditCard.
	self assert: debitedAmout = total.! !

!CashierTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 18:59'!
test06CashierDoesNotSaleWhenTheCreditCardHasNoCredit

	| cart cashier salesBook creditCard |

	cart := testObjectsFactory createCart.
	cart add: testObjectsFactory itemSellByTheStore.
	creditCard := testObjectsFactory notExpiredCreditCard.
	salesBook := OrderedCollection new.
 	debitBehavior := [ :anAmount :aCreditCard | self error: Cashier creditCardHasNoCreditErrorMessage].
	
	cashier:= Cashier 
		toCheckout: cart 
		charging: creditCard
		throught: self
		on: testObjectsFactory today
		registeringOn: salesBook.
		
	self 
		should: [cashier checkOut ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = Cashier creditCardHasNoCreditErrorMessage.
			self assert: salesBook isEmpty ]! !


!CashierTest methodsFor: 'setup' stamp: 'HernanWilkinson 6/17/2013 19:03'!
setUp 

	testObjectsFactory := StoreTestObjectsFactory new.
	debitBehavior := [ :anAmount :aCreditCard | ]! !


!CashierTest methodsFor: 'merchant processor protocol' stamp: 'HernanWilkinson 6/17/2013 19:02'!
debit: anAmount from: aCreditCard 

	^debitBehavior value: anAmount value: aCreditCard ! !


!classDefinition: #FacadeTest category: 'TusLibros'!
TestCase subclass: #FacadeTest
	instanceVariableNames: 'factory clock'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!FacadeTest methodsFor: 'setUp/tearDown' stamp: 'AEJDS 6/13/2022 03:56:47'!
setUp

	factory _ StoreTestObjectsFactory new .
	clock _ factory createClock .
	! !


!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 06:42:53'!
checkOutOn: facade theCart: cartId

	^ facade 
		checkOut: cartId 
		chargingToCreditCardNumber: factory creditCardNumber 
		ofOwner: factory creditCardOwner 
		withAnExpirationDateOn: 	factory notExpiredCreditCardDate! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/12/2022 21:40:12'!
createCartIdFrom: facade

	^ facade 
		createCartWithClientId: factory validClientId
		andPassword: factory validPassword! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/12/2022 19:36:15'!
createDefaultCartFrom: aFacade

	^ aFacade createCartWithClientId: (factory validClientId) andPassword: (factory validPassword) .
	! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 03:56:59'!
createFacade
	^ Facade 
		validatesWith: 		(FakeAuthenticationSystem 
						validFor: factory validClientId
						withPassword: factory validPassword )
		acceptingItemsOf: factory defaultCatalog 
		trackingTimeWith: clock 
		chargingThrough: factory acceptingMerchantProcessor .
			
	! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 07:06:19'!
invalidCartId

	^'An ID that never will be returned.'.
	
! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/12/2022 19:36:37'!
test01RecentlyCreatedCartIsEmpty

	| cartId contents facade |
	facade _ self createFacade.
	
	cartId _ self createDefaultCartFrom: facade.
	
	contents _ facade listCart: cartId.
	
	self assert: contents isEmpty.
	! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 16:36:03'!
test02CannotCreateCartWithInvalidUser

	| cartId facade invalidClientId |
	facade _ self createFacade.

	invalidClientId _ 'a Client ID that is impossible to exist.'.	
	cartId _ self invalidCartId .
	
	self should: [ cartId _ facade createCartWithClientId: invalidClientId 
						       andPassword: (factory validPassword). ]
		raise: Error - MessageNotUnderstood 
		withMessageText: facade invalidUserOrPasswordErrorMessage.

	self assert: self invalidCartId equals: cartId.
	! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/12/2022 19:37:27'!
test03TwoCreatedCartsHasDifferentsIds

	| facade cartIdOne cartIdTwo |
	facade _ self createFacade.
	
	cartIdOne _ self createCartIdFrom: facade.
	cartIdTwo _  self createCartIdFrom: facade.
	
	self deny: cartIdOne = cartIdTwo.
	
	! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/12/2022 19:37:27'!
test04CanAddAValidItemToACartId
	| facade cartId contents  |

	facade _ self createFacade.
	
	cartId _ self createCartIdFrom: facade .
	
	facade add: 3 of: (factory itemSellByTheStore ) to: cartId.
	
	contents _ facade listCart: cartId.
	
	self assert: 3 equals: (contents occurrencesOf: factory itemSellByTheStore ).
	self assert: 3 equals: (contents size).
	! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/12/2022 19:39:13'!
test05TwoDifferentsCartIdsHaveDifferentContents
	| facade cartIdOne cartIdTwo contentsOne contentsTwo validItem |

	facade _ self createFacade.
	validItem _ factory itemSellByTheStore.
	
	cartIdOne _ self createCartIdFrom: facade .
	cartIdTwo _ self createCartIdFrom: facade .
	
	facade add: 3 of: validItem to: cartIdOne.
	facade add: 20 of: validItem to: cartIdTwo.
	
	contentsOne _ facade listCart: cartIdOne.
	contentsTwo _ facade listCart: cartIdTwo.
	
	self assert: 3 equals: (contentsOne occurrencesOf: validItem ).
	self assert: 20 equals: (contentsTwo occurrencesOf: validItem ).
	! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 16:36:21'!
test06CannotListAnInvalidCartId

	| facade |

	facade _ self createFacade.
	
	self should:  [  facade listCart: self 	invalidCartId ]
		raise: Error - MessageNotUnderstood  
		withMessageText: 	facade invaldCartIdErrorMessage.
		
	! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 16:36:21'!
test07CannotAddAValidItemToAnInvalidCartId
	| facade invalidCartId |

	facade _ self createFacade.
	
	invalidCartId _ self invalidCartId .
		
	self should:  [ facade add: 3 of: (factory itemSellByTheStore ) to: invalidCartId. ]
		raise: Error - MessageNotUnderstood 
		withMessageText: facade invaldCartIdErrorMessage .
		
	
	! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/12/2022 21:13:29'!
test08CannotAddAnInvalidItemToACartId
	| facade cartId |

	facade _ self createFacade.

	cartId _ self createCartIdFrom: facade.
		
	self should:  [ facade add: 3 of: (factory itemNotSellByTheStore ) to: cartId. ]
		raise: Error - MessageNotUnderstood 
		withMessageText: facade invalidItemErrorMessage .
		
	
	! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/12/2022 21:13:11'!
test09CannotAddAnInvalidQuantityOfAValidItem

	| facade cartId |

	facade _ self createFacade.

	cartId _ self createCartIdFrom: facade.
		
	self should:  [ facade add: -2  of: (factory itemNotSellByTheStore ) to: cartId. ]
		raise: Error - MessageNotUnderstood 
		withMessageText: facade invalidQuantityErrorMessage .
		
	
	! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 06:45:27'!
test10CannotCheckoutAnEmptyCart

	| facade cartId |

	facade _ self createFacade.

	cartId _ self createCartIdFrom: facade.
		
	self should:  [ self checkOutOn: facade theCart: cartId ]
		raise: Error - MessageNotUnderstood 
		withMessageText: facade cartCanNotBeEmptyErrorMessage .
		
	
	! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 05:48:26'!
test11CannotCheckoutWithAnExpiredCreditCard

	| facade cartId |

	facade _ self createFacade.

	cartId _ self createCartIdFrom: facade.

	facade add: 3 of: factory itemSellByTheStore to: cartId .
	
	self should:  [ facade 
				checkOut: cartId 
				chargingToCreditCardNumber: factory creditCardNumber 
				ofOwner: factory creditCardOwner
		 		withAnExpirationDateOn: factory expiredCreditCardDate . ]
		raise: Error - MessageNotUnderstood 
		withMessageText: facade  canNotChargeAnExpiredCreditCardErrorMessage  .
		! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 07:00:55'!
test12PurchaseListForAClientWithNoPurchasesIsEmpty

	| facade purchases |
	facade _ self createFacade.
	
	purchases _ facade listPurchasesOf: factory validClientId with: factory validPassword .
	
	self assert: purchases isEmpty.
! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 07:01:29'!
test13CanCheckoutAndListPurchases

	| facade cartId purchase quantity |

	facade _ self createFacade.
	cartId _ self createCartIdFrom: facade.
	
	quantity _ 3.
	
	facade add: quantity of: factory itemSellByTheStore to: cartId .

	self checkOutOn: facade theCart: cartId.
	
	purchase _ facade listPurchasesOf: factory validClientId with: factory validPassword .
	
	self assert: quantity equals: purchase size! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 16:36:21'!
test14CannotListTimedOutCartid

	| facade cartId |

	facade _ self createFacade.
	cartId _ self createCartIdFrom: facade.
	
	clock advance: facade timeout + (Duration seconds: 10).
	
	self should: [ facade listCart: cartId ] 
		raise: Error - MessageNotUnderstood 
		withMessageText: facade invaldCartIdErrorMessage .
		! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 07:10:03'!
test15DifferentPurchasesThenTheListPurchasesIsCorrect

	| facade cartId purchase |

	facade _ self createFacade.

	cartId _ self createCartIdFrom: facade.
	facade add: 5 of: (factory itemSellByTheStore ) to: cartId.
	self checkOutOn: facade theCart: cartId .
	
	cartId _ self createCartIdFrom: facade.
	facade add: 12 of: (factory itemSellByTheStore ) to: cartId.
	self checkOutOn: facade theCart: cartId .
		
	purchase _ facade listPurchasesOf: factory validClientId with: factory validPassword .
	
	self assert: 17 equals: purchase size.
	self assert: (purchase allSatisfy: [ :item | item = factory itemSellByTheStore ])
			! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 16:36:21'!
test16AfterCheckoutCartIdIsNotValidAnymore

	| facade cartId |

	facade _ self createFacade.
	cartId _ self createCartIdFrom: facade.
	
	facade add: 5 of: (factory itemSellByTheStore ) to: cartId.
	
	self checkOutOn: facade theCart: cartId .
	
		
	self should: [ facade listCart: cartId ]
		raise: Error - MessageNotUnderstood 
		withMessageText: facade invaldCartIdErrorMessage .
				! !

!FacadeTest methodsFor: 'tests' stamp: 'AEJDS 6/13/2022 07:33:53'!
test17MerchantProcessorReturnsAnErrorAndThePurchaseIsCancelled

	| facade cartId purchases |

	facade _ Facade 
		validatesWith: 		(FakeAuthenticationSystem 
						validFor: factory validClientId
						withPassword: factory validPassword )
		acceptingItemsOf: factory defaultCatalog 
		trackingTimeWith: clock 
		chargingThrough: factory rejectingMerchantProcessor .
			
	
	cartId _ self createCartIdFrom: facade.
	
	facade add: 5 of: (factory itemSellByTheStore ) to: cartId.
	
				
	self should: [ self checkOutOn: facade theCart: cartId . ]
		raise: MerchantProcessorError .
				
	purchases _ facade listPurchasesOf: factory validClientId with: factory validPassword .
	self assert: purchases isEmpty.! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'AEJDS 6/12/2022 20:09:36'!
invalidQuantityErrorMessage
	
	^self class invalidQuantityErrorMessage
	
! !


!Cart methodsFor: 'assertions' stamp: 'AEJDS 6/12/2022 19:54:42'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self class invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'AEJDS 6/12/2022 19:07:45'!
contents
	^ items copy.
	! !

!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 19:09'!
total

	^ items sum: [ :anItem | catalog at: anItem ]! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!Cart class methodsFor: 'error messages' stamp: 'AEJDS 6/12/2022 19:54:18'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart class methodsFor: 'error messages' stamp: 'AEJDS 6/12/2022 20:09:42'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'cart salesBook merchantProcessor creditCard total'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:08'!
calculateTotal

	total := cart total.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'AEJDS 6/13/2022 02:29:40'!
createSale

	^ Sale of: total itemsOfSale: cart contents.
	
! !

!Cashier methodsFor: 'checkout - private' stamp: 'HernanWilkinson 6/17/2013 19:06'!
debitTotal

	merchantProcessor debit: total from: creditCard.
	! !

!Cashier methodsFor: 'checkout - private' stamp: 'AEJDS 6/13/2022 02:27:58'!
registerSale

	salesBook add: self createSale.! !


!Cashier methodsFor: 'checkout' stamp: 'HernanWilkinson 6/17/2013 19:06'!
checkOut

	self calculateTotal.
	self debitTotal.
	self registerSale.

	^ total! !


!Cashier methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:53'!
initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor registeringOn: aSalesBook
	
	cart := aCart.
	creditCard := aCreditCard.
	merchantProcessor := aMerchantProcessor.
	salesBook := aSalesBook! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:22'!
assertIsNotEmpty: aCart 
	
	aCart isEmpty ifTrue: [self error: self cartCanNotBeEmptyErrorMessage ]! !

!Cashier class methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 18:23'!
assertIsNotExpired: aCreditCard on: aDate
	
	(aCreditCard isExpiredOn: aDate) ifTrue: [ self error: self canNotChargeAnExpiredCreditCardErrorMessage ]! !


!Cashier class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:51'!
toCheckout: aCart charging: aCreditCard throught: aMerchantProcessor on: aDate registeringOn: aSalesBook
	
	self assertIsNotEmpty: aCart.
	self assertIsNotExpired: aCreditCard on: aDate.
	
	^self new initializeToCheckout: aCart charging: aCreditCard throught: aMerchantProcessor registeringOn: aSalesBook! !


!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 18:21'!
canNotChargeAnExpiredCreditCardErrorMessage
	
	^'Can not charge an expired credit card'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:56'!
cartCanNotBeEmptyErrorMessage
	
	^'Can not check out an empty cart'! !

!Cashier class methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 19:02'!
creditCardHasNoCreditErrorMessage
	
	^'Credit card has no credit'! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'expiration'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 18:39'!
isExpiredOn: aDate 
	
	^expiration start < (Month month: aDate monthIndex year: aDate yearNumber) start ! !


!CreditCard methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:38'!
initializeExpiringOn: aMonth 
	
	expiration := aMonth ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 18:38'!
expiringOn: aMonth 
	
	^self new initializeExpiringOn: aMonth! !


!classDefinition: #Facade category: 'TusLibros'!
Object subclass: #Facade
	instanceVariableNames: 'authenticationSystem currentId sessions catalog clock allSalesBooks merchantProcessor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Facade methodsFor: 'private' stamp: 'AEJDS 6/12/2022 19:12:36'!
createCart
	^Cart acceptingItemsOf: catalog.
	! !

!Facade methodsFor: 'private' stamp: 'AEJDS 6/13/2022 16:36:21'!
getSession: aCartId

	| session |
	session _ sessions at: aCartId ifAbsent: [ self error: self invaldCartIdErrorMessage ].
	
	(session isExpiredOn: clock now)
		ifTrue: [ self releaseSession: aCartId.
			    self error: self invaldCartIdErrorMessage ].
	
	session updateTimeStamp: clock now.
	^session.
	 ! !

!Facade methodsFor: 'private' stamp: 'AEJDS 6/13/2022 06:33:59'!
releaseSession: aCartId 
	
	sessions removeKey: aCartId .
	! !

!Facade methodsFor: 'private' stamp: 'AEJDS 6/13/2022 06:46:44'!
salesBookFor: aClientId

	^ allSalesBooks at: aClientId 
		ifPresent: [ :salesBook | salesBook ]
		ifAbsent: [ OrderedCollection new]! !


!Facade methodsFor: 'tus libros system protocol' stamp: 'AEJDS 6/13/2022 02:46:34'!
add: aQuantity of: IsbnBook to: aCartId 

	| session |
	session _ self getSession: aCartId .
		 
	(session cart) add: aQuantity of: IsbnBook .
	! !

!Facade methodsFor: 'tus libros system protocol' stamp: 'AEJDS 6/13/2022 07:13:27'!
checkOut: aCartId chargingToCreditCardNumber: aCreditCardNumber ofOwner: creditCardOwner withAnExpirationDateOn: expirationDate   
	
	| session cashier creditCard saleBook |
	session _ self getSession: aCartId .
	
	saleBook _ 	allSalesBooks at: session clientId 
						 ifAbsent: [ OrderedCollection new ].
	
	
	creditCard _ CreditCard expiringOn: expirationDate.

	cashier _ Cashier 
				toCheckout: 	session cart 
				charging: creditCard   
				throught: merchantProcessor  
				on: clock now  
				registeringOn: 	saleBook .		
	
	cashier checkOut.
	allSalesBooks at: session clientId put: saleBook.
			
	self releaseSession: aCartId .
	! !

!Facade methodsFor: 'tus libros system protocol' stamp: 'AEJDS 6/13/2022 05:12:45'!
createCartWithClientId: aClientId andPassword: aPassword 

	self assertThatClient: aClientId isValidWith: aPassword.
	
	currentId _ currentId + 1.
	
	sessions at: currentId put: (Session of: aClientId with: self createCart on: clock now withTimeOutOf: self timeout).
		
	^currentId.
	
	! !

!Facade methodsFor: 'tus libros system protocol' stamp: 'AEJDS 6/13/2022 02:48:14'!
listCart: aCartID 
	
	^ ( self getSession: aCartID ) cart contents.
	
	
	
	
	! !

!Facade methodsFor: 'tus libros system protocol' stamp: 'AEJDS 6/13/2022 07:04:04'!
listPurchasesOf: aClientId with: aPassword 

	self assertThatClient: aClientId isValidWith: aPassword.
	
	^ (self salesBookFor: aClientId) 
		inject: Bag new 
		into: [ :bag :salesBook | bag addAll: salesBook items; yourself ].! !


!Facade methodsFor: 'accessing' stamp: 'AEJDS 6/13/2022 04:06:07'!
timeout
	^ Duration minutes: 30.! !


!Facade methodsFor: 'initialization' stamp: 'AEJDS 6/13/2022 06:38:12'!
initializeValidatesWith: anAuthenticationSystem acceptingItemsOf: aCatalog trackingTimeWith: aClock chargingThrough: aMerchantProcessor    

	currentId _ 0.
	catalog _ aCatalog.	
	authenticationSystem := anAuthenticationSystem.
	clock _ aClock.	
	sessions _ Dictionary new.
	allSalesBooks _ Dictionary new.
	merchantProcessor _ aMerchantProcessor 
	! !


!Facade methodsFor: 'error message' stamp: 'AEJDS 6/12/2022 20:24:26'!
canNotChargeAnExpiredCreditCardErrorMessage
	^ Cashier canNotChargeAnExpiredCreditCardErrorMessage! !

!Facade methodsFor: 'error message' stamp: 'AEJDS 6/12/2022 21:12:13'!
cartCanNotBeEmptyErrorMessage
	
	^Cashier cartCanNotBeEmptyErrorMessage .
	! !

!Facade methodsFor: 'error message' stamp: 'AEJDS 6/13/2022 16:36:21'!
invaldCartIdErrorMessage
	^ 'The cart Id is not valid'.
	! !

!Facade methodsFor: 'error message' stamp: 'AEJDS 6/12/2022 19:59:33'!
invalidItemErrorMessage
	
	^ Cart invalidItemErrorMessage .
	! !

!Facade methodsFor: 'error message' stamp: 'AEJDS 6/12/2022 20:17:07'!
invalidQuantityErrorMessage
	^ Cart invalidQuantityErrorMessage .
	! !

!Facade methodsFor: 'error message' stamp: 'AEJDS 6/13/2022 16:36:03'!
invalidUserOrPasswordErrorMessage
	^ 'Invalid User Or Password. '.
	! !


!Facade methodsFor: 'assertions' stamp: 'AEJDS 6/13/2022 16:36:03'!
assertThatClient: aClientId isValidWith: aPassword

	^ (authenticationSystem validate: aClientId withPassword: aPassword)
		ifFalse: [ self error: self invalidUserOrPasswordErrorMessage ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Facade class' category: 'TusLibros'!
Facade class
	instanceVariableNames: ''!

!Facade class methodsFor: 'instance creation' stamp: 'AEJDS 6/13/2022 03:22:53'!
validatesWith: anAuthenticationSystem acceptingItemsOf: aCatalog trackingTimeWith: aClock chargingThrough: aMerchantProcessor  

	^self new initializeValidatesWith: anAuthenticationSystem acceptingItemsOf: aCatalog trackingTimeWith: aClock chargingThrough: aMerchantProcessor ! !


!classDefinition: #FakeAuthenticationSystem category: 'TusLibros'!
Object subclass: #FakeAuthenticationSystem
	instanceVariableNames: 'clientId password'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!FakeAuthenticationSystem methodsFor: 'initialization' stamp: 'AEJDS 6/12/2022 17:41:08'!
initializeValidFor: aClientId withPassword: aPassword 
	clientId := aClientId.
	password := aPassword.! !


!FakeAuthenticationSystem methodsFor: 'authorization' stamp: 'AEJDS 6/12/2022 17:45:21'!
validate: aClientId withPassword: aPassword 
	
	^ (clientId = aClientId) and: [ password = aPassword ].
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FakeAuthenticationSystem class' category: 'TusLibros'!
FakeAuthenticationSystem class
	instanceVariableNames: ''!

!FakeAuthenticationSystem class methodsFor: 'instance creation' stamp: 'AEJDS 6/12/2022 17:40:43'!
validFor: aClientId withPassword: aPassword 
	^self new initializeValidFor: aClientId withPassword: aPassword ! !


!classDefinition: #FakeMerchantProcessor category: 'TusLibros'!
Object subclass: #FakeMerchantProcessor
	instanceVariableNames: 'block'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!FakeMerchantProcessor methodsFor: 'initialization' stamp: 'AEJDS 6/13/2022 07:18:32'!
initializeOnDebitDo: aBlock

	block _ aBlock.
	! !


!FakeMerchantProcessor methodsFor: 'merchant processor protocol' stamp: 'AEJDS 6/13/2022 07:18:04'!
debit: anAmount from: aCreditCard
	
	block value: anAmount value: aCreditCard.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FakeMerchantProcessor class' category: 'TusLibros'!
FakeMerchantProcessor class
	instanceVariableNames: ''!

!FakeMerchantProcessor class methodsFor: 'instance creation' stamp: 'AEJDS 6/13/2022 07:14:30'!
onDebitDo: aBlock
	^self new initializeOnDebitDo: aBlock
	! !


!classDefinition: #FixedClock category: 'TusLibros'!
Object subclass: #FixedClock
	instanceVariableNames: 'dateAndTime'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!FixedClock methodsFor: 'initialization' stamp: 'AEJDS 6/12/2022 21:09:59'!
initializeOn: aDateAndTime 

	dateAndTime := aDateAndTime.! !


!FixedClock methodsFor: 'accessing' stamp: 'AEJDS 6/12/2022 21:29:11'!
now
	
	^ dateAndTime 
	! !


!FixedClock methodsFor: 'time manipulation' stamp: 'AEJDS 6/13/2022 03:58:36'!
advance: aTime

	dateAndTime _ dateAndTime + aTime.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FixedClock class' category: 'TusLibros'!
FixedClock class
	instanceVariableNames: ''!

!FixedClock class methodsFor: 'accessing' stamp: 'AEJDS 6/12/2022 21:09:37'!
on: aDateAndTime 
	^ self new initializeOn: aDateAndTime! !


!classDefinition: #Sale category: 'TusLibros'!
Object subclass: #Sale
	instanceVariableNames: 'total items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Sale methodsFor: 'total' stamp: 'HernanWilkinson 6/17/2013 18:48'!
total
	
	^ total! !


!Sale methodsFor: 'initialization' stamp: 'AEJDS 6/13/2022 06:07:01'!
accumulateFrom: anotherSale

	total := total + anotherSale total.
	items addAll: anotherSale items.
	
	! !

!Sale methodsFor: 'initialization' stamp: 'AEJDS 6/13/2022 02:37:45'!
initializeTotal: aTotal itemsOfSale: itemsOfSale 

	total := aTotal .
	items _ itemsOfSale .
	! !


!Sale methodsFor: 'accessing' stamp: 'AEJDS 6/13/2022 03:28:04'!
items
	
	^items! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Sale class' category: 'TusLibros'!
Sale class
	instanceVariableNames: ''!

!Sale class methodsFor: 'instance creation' stamp: 'AEJDS 6/13/2022 02:27:28'!
of: aTotal itemsOfSale: itemsOfSale 

	"should assert total is not negative or 0!!"
	^self new initializeTotal: aTotal itemsOfSale: itemsOfSale ! !


!classDefinition: #Session category: 'TusLibros'!
Object subclass: #Session
	instanceVariableNames: 'clientId cart timeStamp timeOut'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Session methodsFor: 'initialization' stamp: 'AEJDS 6/13/2022 05:10:16'!
initializeOf: aClientId with: aCart on: aTime timeOutOf: aDuration   

	clientId := aClientId.
	cart := aCart.
	timeStamp _ aTime.
	timeOut _ aDuration
! !


!Session methodsFor: 'accessing' stamp: 'AEJDS 6/13/2022 02:46:53'!
cart
	^cart! !

!Session methodsFor: 'accessing' stamp: 'AEJDS 6/13/2022 03:06:20'!
clientId
	
	^clientId! !

!Session methodsFor: 'accessing' stamp: 'AEJDS 6/13/2022 05:29:55'!
isExpiredOn: aTime

	^ (self lastTimeStamp + timeOut ) < aTime.
	! !

!Session methodsFor: 'accessing' stamp: 'AEJDS 6/13/2022 04:12:21'!
lastTimeStamp
	^ timeStamp
	! !

!Session methodsFor: 'accessing' stamp: 'AEJDS 6/13/2022 06:37:05'!
updateTimeStamp: aDateAndTime 
	
	timeStamp _ aDateAndTime .
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Session class' category: 'TusLibros'!
Session class
	instanceVariableNames: ''!

!Session class methodsFor: 'instance creation' stamp: 'AEJDS 6/13/2022 05:11:59'!
of: aClientId with: aCart on: aTime withTimeOutOf: aTimeDuration   

	^self new initializeOf: aClientId with: aCart on: aTime timeOutOf: aTimeDuration .
	! !


!classDefinition: #StoreTestObjectsFactory category: 'TusLibros'!
Object subclass: #StoreTestObjectsFactory
	instanceVariableNames: 'today'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStore
	
	^ 'validBook'! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'HernanWilkinson 6/17/2013 18:08'!
itemSellByTheStorePrice
	
	^10! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'AEJDS 6/13/2022 07:31:51'!
rejectingMerchantProcessor
	
	^ FakeMerchantProcessor onDebitDo: 
			[ :debit :creditCard |  MerchantProcessorError new signal: 'Quiz�s no hay fondos. Quiz�s una falla de comunicaci�n.' ].! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'AEJDS 6/12/2022 18:00:21'!
validClientId
	^ 'A valid client id. '.
	! !

!StoreTestObjectsFactory methodsFor: 'items' stamp: 'AEJDS 6/12/2022 18:00:45'!
validPassword
	^ 'A valid password for validClientId. '.
	! !


!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'HernanWilkinson 6/17/2013 18:08'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !

!StoreTestObjectsFactory methodsFor: 'cart' stamp: 'HernanWilkinson 6/17/2013 18:08'!
defaultCatalog
	
	^ Dictionary new
		at: self itemSellByTheStore put: self itemSellByTheStorePrice;
		yourself ! !


!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'AEJDS 6/13/2022 07:15:45'!
acceptingMerchantProcessor
	
	^FakeMerchantProcessor onDebitDo: [ :amount :creditcard | ].
	
	! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'AEJDS 6/13/2022 05:37:21'!
creditCardNumber
	^'1111222233334444'.
	! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'AEJDS 6/13/2022 05:28:00'!
creditCardOwner
	^'Robert Owner'.
	! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'AEJDS 6/13/2022 05:47:02'!
expiredCreditCard
	
	^CreditCard expiringOn: self expiredCreditCardDate .
	! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'AEJDS 6/13/2022 05:44:29'!
expiredCreditCardDate
	^Month month: today monthIndex year: today yearNumber - 1.
	! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'AEJDS 6/13/2022 05:46:41'!
notExpiredCreditCard
	
	^CreditCard expiringOn: self notExpiredCreditCardDate ! !

!StoreTestObjectsFactory methodsFor: 'credit card' stamp: 'AEJDS 6/13/2022 05:40:59'!
notExpiredCreditCardDate
	^Month month: today monthIndex year: today yearNumber + 1.
	! !


!StoreTestObjectsFactory methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 18:37'!
initialize

	today := DateAndTime now! !


!StoreTestObjectsFactory methodsFor: 'date' stamp: 'AEJDS 6/12/2022 21:19:47'!
createClock
	^FixedClock on: today.
	! !

!StoreTestObjectsFactory methodsFor: 'date' stamp: 'HernanWilkinson 6/17/2013 18:37'!
today
	
	^ today! !
