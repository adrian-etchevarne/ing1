!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session inputStream sanchezCustomer perezCustomer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'test data' stamp: 'AEJDS 6/23/2022 15:35:50'!
createPerezCustomer

	| customer |
	customer _ Customer named: 'Juan' andLastName: 'Perez'  identifiedBy: '23-25666777-9' ofType: 'C'.
	
	customer 
		addAddress: (Address in: 1122 of: 'Alem'  town: 'CABA'  zipCode:  1001 province: 'CABA' ).
		
	^customer! !

!ImportTest methodsFor: 'test data' stamp: 'AEJDS 6/23/2022 15:36:01'!
createSanchezCustomer

	| customer |
	customer _ Customer named: 'Pepe' andLastName: 'Sanchez'  identifiedBy: '22333444' ofType: 'D'.
	
	customer 
		addAddress: (Address in: 3322 of: 'San Martin'  town: 'Olivos'  zipCode:  1636 province: 'BsAs' );
		addAddress: (Address in: 888 of: 'Maipu'  town: 'Florida'  zipCode:  1122 province: 'Buenos Aires').
		
	^customer! !

!ImportTest methodsFor: 'test data' stamp: 'AEJDS 6/20/2022 19:43:28'!
dataStreamForTest

	^ ReadStream on: 
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !


!ImportTest methodsFor: 'testing' stamp: 'AEJDS 6/23/2022 15:57:37'!
importCustomers
	
	session beginTransaction.
	
	(CustomerImporter session: session from: inputStream) value. 
		
	session commit.! !

!ImportTest methodsFor: 'testing' stamp: 'AEJDS 6/23/2022 15:06:44'!
numberOfImportedCustomers

	^ (session selectAllOfType: Customer) size! !

!ImportTest methodsFor: 'testing' stamp: 'AEJDS 6/23/2022 15:40:07'!
selectCustomerOfId: idNumber andType: idType

	^ (session select: [:aCustomer | 	
		aCustomer identificationType = idType and: [ 	aCustomer identificationNumber = idNumber]] ofType: Customer) anyOne
	! !

!ImportTest methodsFor: 'testing' stamp: 'AEJDS 6/23/2022 15:41:25'!
test01Import

"	Customer importCustomers "
	
	| importedCustomer  importedCustomer2  |
	
	self importCustomers.
	self assert: 2 equals: self numberOfImportedCustomers.
	
	importedCustomer _ self selectCustomerOfId: sanchezCustomer identificationNumber andType: sanchezCustomer identificationType .
	self assert: (sanchezCustomer hasTheSameFieldsOf: importedCustomer ).
	
	importedCustomer2 _ self selectCustomerOfId: perezCustomer identificationNumber andType: perezCustomer identificationType .
	self assert: (perezCustomer hasTheSameFieldsOf: importedCustomer2)

	! !


!ImportTest methodsFor: 'setUp/tearDown' stamp: 'AEJDS 6/23/2022 15:47:45'!
setUp

	inputStream := self dataStreamForTest.

	session := DataBaseSession for: (Array with: Address with: Customer).
	
	sanchezCustomer _ self createSanchezCustomer. 
	perezCustomer _ self createPerezCustomer .
	! !

!ImportTest methodsFor: 'setUp/tearDown' stamp: 'AEJDS 6/23/2022 15:48:34'!
tearDown

	session close.
	
	inputStream close.
! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province townName provinceName'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !


!Address methodsFor: 'initialization' stamp: 'AEJDS 6/20/2022 19:43:01'!
hasTheSameFieldsOf: anAddress

	^(streetNumber = anAddress streetNumber ) and: [ 
		(streetName = anAddress streetName ) and: [
		(town = anAddress town ) and: [
		(zipCode = anAddress zipCode ) and: [
		(province = anAddress province ) ]]]].! !

!Address methodsFor: 'initialization' stamp: 'AEJDS 6/20/2022 19:29:43'!
initializeIn: aStreetNumber of: aStreetName town: aTownName zipCode: aZipCode province: aProvinceName 

	streetNumber := aStreetNumber.
	streetName := aStreetName.
	town := aTownName.
	zipCode := aZipCode.
	province := aProvinceName.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Address class' category: 'CustomerImporter'!
Address class
	instanceVariableNames: ''!

!Address class methodsFor: 'class initialization' stamp: 'AEJDS 6/20/2022 19:16:43'!
in: aStreetNumber of: aStreetName town: aTownName zipCode: aZipCode province: aProvinceName 
	^self new initializeIn: aStreetNumber of: aStreetName town: aTownName zipCode: aZipCode province: aProvinceName .
	! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !

!Customer methodsFor: 'addresses' stamp: 'AEJDS 6/23/2022 15:25:45'!
hasAddress: anAddress 

	^ addresses anySatisfy: [ :address | address hasTheSameFieldsOf: anAddress ].
	! !

!Customer methodsFor: 'addresses' stamp: 'AEJDS 6/23/2022 15:51:53'!
numberOfAddresses

	^ addresses size! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !


!Customer methodsFor: 'initialization' stamp: 'AEJDS 6/23/2022 15:56:49'!
initializeNamed: aFirstName andLastName: aLastName identifiedBy: aIdentificationNumber ofType: anIdentificationType 
	
	addresses := OrderedCollection new.
	
	firstName := aFirstName.
	lastName := aLastName.
	identificationNumber := aIdentificationNumber.
	identificationType := anIdentificationType.! !


!Customer methodsFor: 'comparison' stamp: 'AEJDS 6/23/2022 15:54:49'!
hasTheSameFieldsOf: aCustomer

	^  (lastName = aCustomer lastName) and: [
		firstName = aCustomer firstName and: [
		identificationNumber = aCustomer identificationNumber and: [
		identificationType  =  aCustomer identificationType ]]]
	and: [ 	addresses allSatisfy: [ :address | aCustomer hasAddress: address ] ]
	and: [ self numberOfAddresses = aCustomer numberOfAddresses ].
	


! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Customer class' category: 'CustomerImporter'!
Customer class
	instanceVariableNames: ''!

!Customer class methodsFor: 'importing' stamp: 'AEJDS 6/23/2022 15:53:16'!
importCustomers

	"
	self importCustomers
	"
	| inputStream session |

	inputStream := StandardFileStream new open: 'input.txt' forWrite: false.
	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction.
	
	(CustomerImporter session: session from: inputStream) value. 

	session commit.
	session close.

	inputStream close.
	! !


!Customer class methodsFor: 'class initialization' stamp: 'AEJDS 6/20/2022 19:02:41'!
named: aFirstName andLastName: aLastName identifiedBy: aIdentificationNumber ofType: anIdentificationType

	^self new initializeNamed: aFirstName andLastName: aLastName identifiedBy: aIdentificationNumber ofType: anIdentificationType.
	! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'session stream'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'AEJDS 6/20/2022 19:52:21'!
initializeSession: aSession from: aStream
	session := aSession.
	stream := aStream.! !


!CustomerImporter methodsFor: 'evaluating' stamp: 'AEJDS 6/20/2022 19:52:21'!
value

	| inputStream  newCustomer line |

	inputStream := stream.

	line := inputStream nextLine.
	[ line notNil ] whileTrue: [
		(line beginsWith: 'C') ifTrue: [ | customerData |
			customerData := line findTokens: $,.
			
			newCustomer _ Customer 
							named: customerData second
							andLastName: customerData third
							identifiedBy: customerData fifth 
							ofType: customerData fourth.

			session persist: newCustomer ].

		(line beginsWith: 'A') ifTrue: [ | addressData |
			addressData := line findTokens: $,.
			
			newCustomer addAddress: 
				(Address in: addressData third asNumber
						  of: addressData second
						  town: addressData fourth
						  zipCode: addressData fifth asNumber
						  province: addressData sixth ).			].
					
		line := inputStream nextLine. ].

	inputStream close.
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'AEJDS 6/20/2022 19:52:21'!
session: aSession from: aStream
	^self new initializeSession: aSession from: aStream! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 19:17:36'!
commit

	(tables at: Customer ifAbsent: [#()]) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close

	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 19:29:06'!
objectsOfType: aType

	^ tables at: aType ifAbsent: [ #() ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |

	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].

	self defineIdOf: anObject.
	table add: anObject.

	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
select: aCondition ofType: aType

	self delay.
	^(self objectsOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
selectAllOfType: aType

	self delay.
	^(self objectsOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
