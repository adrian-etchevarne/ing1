!classDefinition: #ImportTest category: 'CustomerImporter'!
TestCase subclass: #ImportTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!ImportTest methodsFor: 'tests' stamp: 'HAW 5/22/2022 18:17:51'!
test01Import

	CustomerImporter valueFrom: self validImportData into: session..

	self assertImportedRightNumberOfCustomers.
	self assertPepeSanchezWasImportedCorrecty.
	self assertJuanPerezWasImportedCorrectly ! !

!ImportTest methodsFor: 'tests' stamp: 'AEJDS 6/27/2022 14:37:16'!
test02RecordWithAnInvalidTypeStartingWithCShouldRaiseAnError

	self should: [
		CustomerImporter valueFrom: self invalidRecordTypeData into: session. ]
	
		raise: Error - MessageNotUnderstood
		withMessageText: CustomerImporter invalidRecordTypeErrorDescription.
		
	self assertThatThereIsNoCustomers
	

! !

!ImportTest methodsFor: 'tests' stamp: 'AEJDS 6/27/2022 13:48:22'!
test03RecordWithAnInvalidTypeStartingWithAShouldRaiseAnError

	| importedCustomer |
	self should: [
		CustomerImporter valueFrom: self invalidRecordTypeStartingWithAData into: session. ]
	
		raise: Error - MessageNotUnderstood
		withMessageText: CustomerImporter invalidRecordTypeErrorDescription.

	importedCustomer := self customerWithIdentificationType: 'D' number: '22333444'.

	self assert: 0 equals: (importedCustomer addresses size)
	

! !

!ImportTest methodsFor: 'tests' stamp: 'AEJDS 6/27/2022 14:37:16'!
test04CustomerRecordShouldHaveTheRightNumberOfFields
	
	self should: [
		CustomerImporter valueFrom: self invalidCustomerRecordWithWrongNumberOfFieldsData into: session. ]
	
		raise: Error - MessageNotUnderstood
		withMessageText: CustomerImporter wrongNumberOfFieldsErrorDescription.

	self assertThatThereIsNoCustomers

! !

!ImportTest methodsFor: 'tests' stamp: 'AEJDS 6/27/2022 14:15:10'!
test05AddressRecordShouldHaveTheRightNumberOfFields
	
	| importedCustomer |
	self should: [
		CustomerImporter valueFrom: self invalidAddressRecordWithWrongNumberOfFieldsData into: session. ]
	
		raise: Error - MessageNotUnderstood
		withMessageText: CustomerImporter wrongNumberOfFieldsErrorDescription.

	importedCustomer := self customerWithIdentificationType: 'D' number: '22333444'.

	self assert: 0 equals: (importedCustomer addresses size)
! !

!ImportTest methodsFor: 'tests' stamp: 'AEJDS 6/27/2022 14:39:19'!
test06AddressRecordShouldHaveACustomerBeforeIt
	
	
	self should: [
		CustomerImporter valueFrom: self addressWithoutACustomerRecordData into: session. ]
	
		raise: Error - MessageNotUnderstood
		withMessageText: CustomerImporter addressRecordDoesNotHaveACustomerErrorDescription.

	self assertThatThereIsNoCustomers
	! !

!ImportTest methodsFor: 'tests' stamp: 'AEJDS 6/27/2022 14:41:35'!
test07RecordFieldsShouldNotBeEmpty
	
	self should: [
		CustomerImporter valueFrom: self customerWithEmptyFieldsData into: session. ]
	
		raise: Error - MessageNotUnderstood
		withMessageText: CustomerImporter recordHasEmptyFieldsErrorDescription.

	self assertThatThereIsNoCustomers
	! !


!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:22:05'!
assertAddressOf: importedCustomer at: aStreetName hasNumber: aNumber town: aTown zipCode: aZipCode province: aProvince

	| importedAddress |

	importedAddress := importedCustomer addressAt: aStreetName ifNone: [ self fail ].
	self assert: aStreetName equals: importedAddress streetName.
	self assert: aNumber equals: importedAddress streetNumber.
	self assert: aTown equals: importedAddress town.
	self assert: aZipCode equals: importedAddress zipCode.
	self assert: aProvince equals: importedAddress province.

	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:27:57'!
assertCustomerWithIdentificationType: anIdType number: anIdNumber hasFirstName: aFirstName lastName: aLastName

	| importedCustomer |

	importedCustomer := self customerWithIdentificationType: anIdType number: anIdNumber.

	self assert: aFirstName equals: importedCustomer firstName.
	self assert: aLastName equals: importedCustomer lastName.
	self assert: anIdType equals: importedCustomer identificationType.
	self assert: anIdNumber equals: importedCustomer identificationNumber.

	^importedCustomer

	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:12:18'!
assertImportedRightNumberOfCustomers

	^ self assert: 2 equals: (session selectAllOfType: Customer) size! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:45'!
assertJuanPerezWasImportedCorrectly

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'C' number: '23-25666777-9' hasFirstName: 'Juan' lastName: 'Perez'.
	self assertAddressOf: importedCustomer at: 'Alem' hasNumber: 1122 town: 'CABA' zipCode: 1001 province: 'CABA'
	! !

!ImportTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:05'!
assertPepeSanchezWasImportedCorrecty

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'D' number: '22333444' hasFirstName: 'Pepe' lastName: 'Sanchez'.
	self assertAddressOf: importedCustomer at: 'San Martin' hasNumber: 3322 town: 'Olivos' zipCode: 1636 province: 'BsAs'.
	self assertAddressOf: importedCustomer at: 'Maipu' hasNumber: 888 town: 'Florida' zipCode: 1122 province: 'Buenos Aires'.


	! !

!ImportTest methodsFor: 'assertions' stamp: 'AEJDS 6/27/2022 14:37:16'!
assertThatThereIsNoCustomers

	^ self assert: 0 equals: (session selectAllOfType: Customer) size! !


!ImportTest methodsFor: 'setUp/tearDown' stamp: 'HAW 5/22/2022 00:27:50'!
setUp

	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction.
! !

!ImportTest methodsFor: 'setUp/tearDown' stamp: 'HAW 5/22/2022 00:28:23'!
tearDown

	session commit.
	session close.
	! !


!ImportTest methodsFor: 'customer' stamp: 'HAW 5/22/2022 18:14:22'!
customerWithIdentificationType: anIdType number: anIdNumber

	^ (session
		select: [ :aCustomer | aCustomer identificationType = anIdType and: [ aCustomer identificationNumber = anIdNumber ]]
		ofType: Customer) anyOne! !


!ImportTest methodsFor: 'test data' stamp: 'AEJDS 6/27/2022 14:39:19'!
addressWithoutACustomerRecordData

	^ ReadStream on: 'A,San Martin,3322,Olivos,1636,BsAs'
! !

!ImportTest methodsFor: 'test data' stamp: 'AEJDS 6/27/2022 14:43:29'!
customerWithEmptyFieldsData
	^ ReadStream on:
'C,Pepe, , ,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !

!ImportTest methodsFor: 'test data' stamp: 'AEJDS 6/27/2022 14:14:43'!
invalidAddressRecordWithWrongNumberOfFieldsData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs,onemore'
! !

!ImportTest methodsFor: 'test data' stamp: 'AEJDS 6/27/2022 14:04:15'!
invalidCustomerRecordWithWrongNumberOfFieldsData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444,onemore'
! !

!ImportTest methodsFor: 'test data' stamp: 'AEJDS 6/23/2022 20:41:18'!
invalidRecordTypeData

	^ ReadStream on:
'CC,Pepe,Sanchez,D,22333444' .! !

!ImportTest methodsFor: 'test data' stamp: 'AEJDS 6/27/2022 13:39:53'!
invalidRecordTypeStartingWithAData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
AA,San Martin,3322,Olivos,1636,BsAs'
! !

!ImportTest methodsFor: 'test data' stamp: 'HAW 5/22/2022 18:08:08'!
validImportData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 17:55:46'!
isAt: aStreetName

	^streetName = aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 17:55:17'!
addressAt: aStreetName ifNone: aNoneBlock

	^addresses detect: [ :address | address isAt: aStreetName ] ifNone: aNoneBlock ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'session readStream line record newCustomer newAddress'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'HAW 5/22/2022 18:06:47'!
initializeFrom: aReadStream into: aSession
	session := aSession.
	readStream := aReadStream.! !


!CustomerImporter methodsFor: 'signaling' stamp: 'AE 6/27/2022 15:22:19'!
signalAddressRecordDoesNotHaveACustomerError

	^ self error: self class addressRecordDoesNotHaveACustomerErrorDescription! !

!CustomerImporter methodsFor: 'signaling' stamp: 'AE 6/27/2022 15:17:38'!
signalInvalidRecordTypeError

	^ self error: self class invalidRecordTypeErrorDescription! !

!CustomerImporter methodsFor: 'signaling' stamp: 'AE 6/27/2022 15:16:36'!
signalRecordHasEmptyFieldsError

	^ self error:  self class recordHasEmptyFieldsErrorDescription! !

!CustomerImporter methodsFor: 'signaling' stamp: 'AE 6/27/2022 15:21:41'!
signalWrongNumberOfFieldsError

	^ self error: self class wrongNumberOfFieldsErrorDescription! !


!CustomerImporter methodsFor: 'evaluating' stamp: 'AEJDS 6/27/2022 14:30:32'!
createAddressFromRecord

	self assertAddressRecordHasTheRightNumberOfFields.
	
	self assertAddressHasAMatchingCustomer.
		
	newAddress := Address new.
			newCustomer addAddress: newAddress.
			newAddress streetName: record second.
			newAddress streetNumber: record third asNumber .
			newAddress town: record fourth.
			newAddress zipCode: record fifth asNumber .
			newAddress province: record sixth! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'AEJDS 6/27/2022 14:05:55'!
createCustomerFromRecord

	self assertCustomerRecordHasTheRightNumberOfFields.
		
	newCustomer := Customer new.
			newCustomer firstName: record second.
			newCustomer lastName: record third.
			newCustomer identificationType: record fourth.
			newCustomer identificationNumber: record fifth.
			session persist: newCustomer! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'AE 6/27/2022 15:15:10'!
createRecord

	record := ( line findTokens: $, )  collect: [ :field | field withBlanksTrimmed ].
	
	self assertFieldsAreNotEmpty.
	! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'AE 6/27/2022 15:20:41'!
importCustomerFromRecord

	^ (self isACustomerRecord ) ifTrue: [  
			self createCustomerFromRecord ]! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'AE 6/27/2022 15:23:20'!
importFromRecord

	 (self isACustomerRecord ) ifTrue: [  ^ 			self createCustomerFromRecord ].
	(self isAnAddressRecord) ifTrue: [   		^ self createAddressFromRecord ].

	self signalInvalidRecordTypeError .
	! !

!CustomerImporter methodsFor: 'evaluating' stamp: 'AE 6/27/2022 15:19:38'!
value

	[ self isThereANextLine ] whileTrue: [
		self createRecord.
		self importFromRecord.
      ].

	! !


!CustomerImporter methodsFor: 'testing' stamp: 'AE 6/27/2022 15:20:41'!
isACustomerRecord

	^ record first = 'C'.
	! !

!CustomerImporter methodsFor: 'testing' stamp: 'AE 6/27/2022 15:23:20'!
isAnAddressRecord

	^ record first = 'A'! !

!CustomerImporter methodsFor: 'testing' stamp: 'AE 6/27/2022 15:19:38'!
isThereANextLine

	line := readStream nextLine.
	^ line notNil! !


!CustomerImporter methodsFor: 'assertions' stamp: 'AE 6/27/2022 15:22:19'!
assertAddressHasAMatchingCustomer

	^ newCustomer isNil ifTrue: [ self signalAddressRecordDoesNotHaveACustomerError. ]! !

!CustomerImporter methodsFor: 'assertions' stamp: 'AE 6/27/2022 15:21:41'!
assertAddressRecordHasTheRightNumberOfFields

	^ (record size = 6) 
		ifFalse: [ self signalWrongNumberOfFieldsError ]! !

!CustomerImporter methodsFor: 'assertions' stamp: 'AE 6/27/2022 15:21:41'!
assertCustomerRecordHasTheRightNumberOfFields

	^ (record size = 5)
		ifFalse: [ self signalWrongNumberOfFieldsError ]! !

!CustomerImporter methodsFor: 'assertions' stamp: 'AE 6/27/2022 15:16:36'!
assertFieldsAreNotEmpty

	^ (record allSatisfy: [ :field | field size > 0 ])
		ifFalse: [ self signalRecordHasEmptyFieldsError ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 18:06:47'!
from: aReadStream into: aSession
	^self new initializeFrom: aReadStream into: aSession! !


!CustomerImporter class methodsFor: 'importing' stamp: 'HAW 5/22/2022 18:11:27'!
valueFrom: aReadStream into: aSession

	^(self from: aReadStream into: aSession) value! !


!CustomerImporter class methodsFor: 'error handling' stamp: 'AEJDS 6/23/2022 20:38:02'!
invalidRecordTypeErrorDescription
	^ 'Invalid Record Type'.
	! !


!CustomerImporter class methodsFor: 'error description' stamp: 'AEJDS 6/27/2022 14:27:33'!
addressRecordDoesNotHaveACustomerErrorDescription
	^ 'Address record does not have a matching Customer'.! !

!CustomerImporter class methodsFor: 'error description' stamp: 'AEJDS 6/27/2022 14:39:51'!
recordHasEmptyFieldsErrorDescription
	^ 'Record has empty fields.'.
	! !

!CustomerImporter class methodsFor: 'error description' stamp: 'AEJDS 6/27/2022 13:56:06'!
wrongNumberOfFieldsErrorDescription
	^'Record has the wrong number of fields.'.
	 ! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 19:17:36'!
commit

	(tables at: Customer ifAbsent: [#()]) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close

	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 19:29:06'!
objectsOfType: aType

	^ tables at: aType ifAbsent: [ #() ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |

	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].

	self defineIdOf: anObject.
	table add: anObject.

	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
select: aCondition ofType: aType

	self delay.
	^(self objectsOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
selectAllOfType: aType

	self delay.
	^(self objectsOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
