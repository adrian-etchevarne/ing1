!classDefinition: #LadderSlidesTest category: 'ISW1-2022-1C-2doParcial'!
TestCase subclass: #LadderSlidesTest
	instanceVariableNames: 'boardSize standarStartingPositions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!LadderSlidesTest methodsFor: 'setUp/tearDown' stamp: 'AE 7/4/2022 21:52:18'!
createGameOfSize: aSize andDice: aDice

	^ self createGameOfSize:  aSize startingPositions:  standarStartingPositions andDice: aDice.
	! !

!LadderSlidesTest methodsFor: 'setUp/tearDown' stamp: 'AE 7/4/2022 21:38:03'!
createGameOfSize: aSize startingPositions: startingPositions andDice: aDice

	| finalPositions ladders squareSize slides |

	squareSize := aSize@aSize.

	finalPositions _ OrderedCollection with: (LSPosition at: squareSize on: 1) with: (LSPosition at: squareSize on: 1). 
	ladders _ OrderedCollection with: (Shortcut ladderFrom: (LSPosition at: 3@3 on: 1) to: (LSPosition at: 4@4 on: 2)).
	slides _ OrderedCollection with: (Shortcut slideFrom: (LSPosition at: 3@3 on: 2) to: (LSPosition at: 4@2 on: 1)).

	^ LadderSlides playingOn: aSize@aSize startingPositions: startingPositions tryingToReach: finalPositions withLadders: ladders andSlides: slides throwing: aDice.
	! !

!LadderSlidesTest methodsFor: 'setUp/tearDown' stamp: 'AE 7/4/2022 21:52:00'!
createGameOfSize: aSize withLadders: ladders andDice: aDice

	| finalPositions squareSize slides  |

	squareSize := aSize@aSize.

	finalPositions _ OrderedCollection with: (LSPosition at: squareSize on: 1) with: (LSPosition at: squareSize on: 1). 
	slides _ OrderedCollection with: (Shortcut slideFrom: (LSPosition at: 3@3 on: 2) to: (LSPosition at: 4@2 on: 1)).

	^ LadderSlides playingOn: aSize@aSize startingPositions: standarStartingPositions tryingToReach: finalPositions withLadders: ladders andSlides: slides throwing: aDice.
	! !

!LadderSlidesTest methodsFor: 'setUp/tearDown' stamp: 'AE 7/4/2022 21:54:33'!
setUp

	standarStartingPositions _ OrderedCollection with: (LSPosition at: 1@1 on: 1) with:  (LSPosition at: 1@1 on: 1)! !


!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 21:11:05'!
test01PlayerOneDoesNotMoveWhenDistanceIsZeroAtZero

	| game dice |
	dice _ TestingDice1D12 gives: #(6 6).
	game _ self createGameOfSize: 5 andDice: dice. 
	game currentPlayerThrowDice.

	self assert: (game player: 1 isAt: (LSPosition at: 1@1 on: 1)).
	self assert: 2 equals: game playerToThrow.
	self deny: (game hasPlayerWon: 1).
	! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 20:56:29'!
test02PlayerOneDoesMoveWhenDistanceIsNonZero

	| game dice |
	dice _ TestingDice1D12 gives: #(7 7).

	game _ self createGameOfSize: 5 andDice: dice. 

	game currentPlayerThrowDice.
	
	self assert: (game player: 1 isAt: (LSPosition at: 2@2 on: 1)).
	self deny: (game player: 1 isAt: (LSPosition at: 1@1 on: 1)).

	self assert: 2 equals: game playerToThrow.
		
	self deny: (game hasPlayerWon: 1).
	self deny: (game hasPlayerWon: 2).
	! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 21:11:32'!
test03PlayerTwoDoesMoveWhenDistanceIsNonZero

	| game dice |
	dice _ TestingDice1D12 gives: #(6 6 7 7).

	game _ self createGameOfSize: 5 andDice: dice. 

	game currentPlayerThrowDice.
	game currentPlayerThrowDice.
	
	self assert: (game player: 2 isAt: (LSPosition at: 2@2 on: 1)).
	self assert: (game player: 1 isAt: (LSPosition at: 1@1 on: 1)).
! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 21:12:00'!
test04PlayerDoesNotMoveOutOfBoundsOnLeftBottom

	| game dice |
	dice _ TestingDice1D12 gives: #(4 2).
	game _ self createGameOfSize: 5 andDice: dice. 

	game currentPlayerThrowDice.
	
	self assert: (game player: 1 isAt: (LSPosition at: 1@1 on: 1)).
	! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 21:12:23'!
test05PlayerDoesNotMoveOutOfBoundsOnRightUp

	| game  dice |
	
	dice _ TestingDice1D12 gives: #(12 12).
	game _ self createGameOfSize: 5 andDice: dice. 

	game currentPlayerThrowDice .

	self assert: (game player: 1 isAt: (LSPosition at: 5@5 on: 1))! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 20:00:24'!
test06PlayerOneGoesAfterPlayerTwoOnTwoPlayerGames

	| game  dice |
	
	dice _ TestingDice1D12 gives: #(6 6 6 6).
		game _ self createGameOfSize: 5 andDice: dice. 

	game currentPlayerThrowDice .
	game currentPlayerThrowDice .
	
	self assert: 1 equals: game playerToThrow.
	! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 20:18:59'!
test07PlayerMovesAndEndOnFinalPositionThenWins

	| game  dice |
	
	dice _ TestingDice1D12 gives: #(10 10).
	game _ self createGameOfSize: 5 andDice: dice. 

	game currentPlayerThrowDice .
	
	self assert: (game hasPlayerWon: 1).
	! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 20:05:32'!
test08PlayerTwoMovesAndEndOnFinalPositionThenWins

	| game  dice |
	
	dice _ TestingDice1D12 gives: #(6 6 10 10).
	game _ self createGameOfSize: 5 andDice: dice. 

	game currentPlayerThrowDice .
	game currentPlayerThrowDice .
	
	self deny: (game hasPlayerWon: 1).
	self assert: (game hasPlayerWon: 2).
! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 20:18:27'!
test09PlayersCanStartOnDifferentPositions
	| game  dice |
	
	dice _ TestingDice1D12 gives: #(6 6 10 10).
	game _ self createGameOfSize: 5 andDice: dice. 

	game currentPlayerThrowDice .
	game currentPlayerThrowDice .
	
	self deny: (game hasPlayerWon: 1).
	self assert: (game hasPlayerWon: 2).
! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 21:05:55'!
test10PlayerReachALadderAndEndsOnUpperFloor
	| game  dice ladderEnd |
	
	dice _ TestingDice1D12 gives: #(8 8).

	ladderEnd _ LSPosition at: 4@4 on: 2.
	
	game _ self createGameOfSize: 5 andDice: dice. 

	game currentPlayerThrowDice .

	self assert: (game player: 1 isAt: ladderEnd).
	 ! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 21:32:26'!
test11PlayerReachASlideAndEndsOnBottomFloor
	| game  dice slideEnd startingPositions |
	
	dice _ TestingDice1D12 gives: #(8 8).

	slideEnd _ LSPosition at: 4@2 on: 1.
	
	startingPositions _ OrderedCollection with: (LSPosition at: 1@1 on: 2) with: (LSPosition at: 1@1 on: 1).
	
	game _ self createGameOfSize: 5 startingPositions: startingPositions andDice: dice. 

	game currentPlayerThrowDice .

	self assert: (game player: 1 isAt: slideEnd).
	 ! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 21:40:32'!
test12LadderCannotGoDown
	| endPos startingPos |
	
	startingPos _ LSPosition at: 1@1 on: 2.
	endPos _ LSPosition at: 1@1 on: 1.
	
	self should: [   Shortcut ladderFrom: startingPos to: endPos ]
		raise: Error - MessageNotUnderstood
		withMessageText: Shortcut ladderCannotGoDownErrorDescription.
		 ! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 21:46:02'!
test13SlideCannotGoUp
	| endPos startingPos |
	
	startingPos _ LSPosition at: 1@1 on: 1.
	endPos _ LSPosition at: 1@1 on: 2.
	
	self should: [   Shortcut slideFrom: startingPos to: endPos ]
		raise: Error - MessageNotUnderstood
		withMessageText: Shortcut slideCannotGoUpErrorDescription.
		 ! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 21:59:45'!
todo_test14ThereIsOnlyOneStartingShortcutOnPositionAtMost
	| finalPositions ladders squareSize slides finalPosition startingPositions dice |
	
	dice _ TestingDice1D12 gives: #( ).
	squareSize _ 5@5.
	finalPosition _ (LSPosition at: squareSize on: 1).
	finalPositions _ OrderedCollection with: finalPosition with: finalPosition. 
	startingPositions _ OrderedCollection with: (LSPosition at: 1@1 on: 1) with: (LSPosition at: 4@4 on: 2).

	ladders _ OrderedCollection with: (Shortcut ladderFrom: (LSPosition at: 3@3 on: 1) to: (LSPosition at: 4@4 on: 2)).
	slides _ OrderedCollection with: (Shortcut slideFrom: (LSPosition at: 3@3 on: 2) to: (LSPosition at: 4@2 on: 1)).

	self should: [ LadderSlides playingOn: squareSize startingPositions: startingPositions tryingToReach: finalPositions withLadders: ladders andSlides: slides throwing: dice. ]
	! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 21:48:44'!
todo_test_PlayerCanMoveOnFloorsOfDifferentSize
! !

!LadderSlidesTest methodsFor: 'tests' stamp: 'AE 7/4/2022 21:49:26'!
todo_test_thereIsOnlyOneStartingShortcutOnPositionAtMost

! !


!classDefinition: #LSPosition category: 'ISW1-2022-1C-2doParcial'!
Object subclass: #LSPosition
	instanceVariableNames: 'position floorNumber'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!LSPosition methodsFor: 'initialization' stamp: 'AE 7/4/2022 19:36:57'!
floorNumber
	^ floorNumber! !

!LSPosition methodsFor: 'initialization' stamp: 'AE 7/4/2022 19:33:24'!
initializeAt: aPosition on: aFloorNumber 
	
	position := aPosition.
	floorNumber := aFloorNumber.! !

!LSPosition methodsFor: 'initialization' stamp: 'AE 7/4/2022 19:36:37'!
position	
	^position ! !


!LSPosition methodsFor: 'moving' stamp: 'AE 7/4/2022 21:10:39'!
advance: aPositionDistance within: aMaximumPositionSize

	| newPosition |

	newPosition := position .
	newPosition _ newPosition + aPositionDistance.
	newPosition x < 1 ifTrue:
		[ newPosition _ 1@(newPosition y) ].

	newPosition y < 1 ifTrue:
		[ newPosition _ (newPosition x)@1  ].
		
	newPosition x > (aMaximumPositionSize x) ifTrue:
		[ newPosition _ (aMaximumPositionSize x)@(newPosition y) ].

	newPosition y > (aMaximumPositionSize y) ifTrue:
		[ newPosition _ (newPosition x)@(aMaximumPositionSize y) ].
		
	^ LSPosition at: newPosition on: floorNumber .
	
	! !


!LSPosition methodsFor: 'accessing' stamp: 'AE 7/4/2022 19:38:25'!
isEqualTo: aLSPosition

	^ (floorNumber = aLSPosition floorNumber ) and: [ position = aLSPosition position ].
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'LSPosition class' category: 'ISW1-2022-1C-2doParcial'!
LSPosition class
	instanceVariableNames: ''!

!LSPosition class methodsFor: 'instance creation' stamp: 'AE 7/4/2022 19:32:54'!
at: aPoint on: aFloorNumber 
	^self new initializeAt: aPoint on: aFloorNumber ! !


!classDefinition: #LadderSlides category: 'ISW1-2022-1C-2doParcial'!
Object subclass: #LadderSlides
	instanceVariableNames: 'positions board dice currentPlayer finalPositions ladders slides'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!LadderSlides methodsFor: 'private - moving players' stamp: 'AE 7/4/2022 21:21:29'!
movePlayer: aPlayer distance: aDistance 
	
	| actualPosition endPosition ladderAtPosition slideAtPosition |

	actualPosition := positions at: aPlayer.
		
	endPosition _ actualPosition advance: aDistance within: board .

	positions at: aPlayer put: endPosition.
	
	ladderAtPosition _ ladders detect: [ :ladder | ladder isStartingAt: endPosition ]
 						    ifFound: [ :ladder | 	positions at: aPlayer put: (ladder endingPosition)]
						    ifNone: [ ].
						
	slideAtPosition _ slides detect: [ :slide | slide isStartingAt: endPosition ]
					      ifFound: [ :slide| 	positions at: aPlayer put: (slide endingPosition)]
						ifNone: [].
						! !


!LadderSlides methodsFor: 'accessing' stamp: 'AE 7/4/2022 20:04:13'!
hasPlayerWon: aPlayer 
	
	| playerPosition |
	playerPosition _ (positions at: aPlayer).
	
	^playerPosition isEqualTo: (finalPositions at: aPlayer).
	
	! !

!LadderSlides methodsFor: 'accessing' stamp: 'AE 7/4/2022 19:39:07'!
player: aPlayer isAt: aLSPosition 

	^ (positions at: aPlayer) isEqualTo: aLSPosition 
! !

!LadderSlides methodsFor: 'accessing' stamp: 'AE 7/4/2022 18:56:37'!
playerToThrow
	^ currentPlayer ! !


!LadderSlides methodsFor: 'initialization' stamp: 'AE 7/4/2022 21:08:51'!
initialize
	positions _ OrderedCollection with: (LSPosition at: 1@1 on: 1) with: (LSPosition at: 1@1 on: 1).
	
	! !

!LadderSlides methodsFor: 'initialization' stamp: 'AE 7/4/2022 21:25:06'!
initializePlayingOn: aBoard startingPositions: startingPositions tryingToReach: aCollectionOfFinalPositions withLadders: aCollectionOfLadders andSlides: aCollectionOfSlides throwing: aDice  

	
	board := aBoard.
	dice _ aDice.
	
	ladders _ aCollectionOfLadders .
	slides _ aCollectionOfSlides .
	
	positions _ startingPositions .
	finalPositions _ aCollectionOfFinalPositions .
	currentPlayer _ 1.
	! !


!LadderSlides methodsFor: 'playing the game' stamp: 'AE 7/4/2022 19:00:25'!
currentPlayerThrowDice
	
	| throwForX throwForY |
	throwForX _ dice throw.
	throwForY _ dice throw.
	
	self movePlayer: currentPlayer distance: (throwForX@throwForY - (6@6)).
	
	currentPlayer _ (currentPlayer mod: 2) + 1
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'LadderSlides class' category: 'ISW1-2022-1C-2doParcial'!
LadderSlides class
	instanceVariableNames: ''!

!LadderSlides class methodsFor: 'instance creation' stamp: 'AE 7/4/2022 21:25:33'!
playingOn: aBoard startingPositions: startingPositions tryingToReach: aCollectionOfFinalPositions withLadders: aCollectionOfLadders andSlides: aCollectionOfSlides throwing: dice 

	^self new initializePlayingOn: aBoard startingPositions: startingPositions tryingToReach: aCollectionOfFinalPositions withLadders: aCollectionOfLadders andSlides: aCollectionOfSlides throwing: dice ! !


!classDefinition: #Shortcut category: 'ISW1-2022-1C-2doParcial'!
Object subclass: #Shortcut
	instanceVariableNames: 'startingLSPosition finishingLSPosition'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!Shortcut methodsFor: 'initialization' stamp: 'AE 7/4/2022 20:55:08'!
initializeFrom: aStartingLSPosition to: anFinishingLSPosition 

	startingLSPosition := aStartingLSPosition.
	finishingLSPosition := anFinishingLSPosition.! !


!Shortcut methodsFor: 'accessing' stamp: 'AE 7/4/2022 21:06:12'!
endingPosition
	^ finishingLSPosition ! !

!Shortcut methodsFor: 'accessing' stamp: 'AE 7/4/2022 20:55:40'!
isStartingAt: aLSPosition 
	
	^ (startingLSPosition isEqualTo: aLSPosition ).
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Shortcut class' category: 'ISW1-2022-1C-2doParcial'!
Shortcut class
	instanceVariableNames: ''!

!Shortcut class methodsFor: 'instance creation' stamp: 'AE 7/4/2022 21:43:32'!
ladderFrom: aStartingLSPosition to: anFinishingLSPosition 
	
	self assertThatLaddersGoUp: aStartingLSPosition to: anFinishingLSPosition .
	
	^self new initializeFrom: aStartingLSPosition to: anFinishingLSPosition ! !

!Shortcut class methodsFor: 'instance creation' stamp: 'AE 7/4/2022 21:45:09'!
slideFrom: aStartingLSPosition to: anFinishingLSPosition 


	self assertThatSlidesGoDown: aStartingLSPosition to: anFinishingLSPosition .
	
	^self new initializeFrom: aStartingLSPosition to: anFinishingLSPosition ! !


!Shortcut class methodsFor: 'error messages' stamp: 'AE 7/4/2022 21:41:09'!
ladderCannotGoDownErrorDescription
	^ 'Ladder cannot go down'.
	! !

!Shortcut class methodsFor: 'error messages' stamp: 'AE 7/4/2022 21:44:36'!
slideCannotGoUpErrorDescription
	^ 'Slides cannot go up'.
	! !


!Shortcut class methodsFor: 'assertions' stamp: 'AE 7/4/2022 21:43:12'!
assertThatLaddersGoUp: aStartingLSPosition to: anFinishingLSPosition

	(aStartingLSPosition floorNumber >= anFinishingLSPosition floorNumber )
		ifTrue: [ self error: self ladderCannotGoDownErrorDescription ].
		! !

!Shortcut class methodsFor: 'assertions' stamp: 'AE 7/4/2022 21:45:35'!
assertThatSlidesGoDown: aStartingLSPosition to: anFinishingLSPosition

	(aStartingLSPosition floorNumber <= anFinishingLSPosition floorNumber )
		ifTrue: [ self error: self slideCannotGoUpErrorDescription ].
		! !


!classDefinition: #TestingDice1D12 category: 'ISW1-2022-1C-2doParcial'!
Object subclass: #TestingDice1D12
	instanceVariableNames: 'collectionOfDiceValues'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ISW1-2022-1C-2doParcial'!

!TestingDice1D12 methodsFor: 'playing' stamp: 'AE 7/4/2022 18:29:33'!
throw
	^ collectionOfDiceValues next! !


!TestingDice1D12 methodsFor: 'initialization' stamp: 'AE 7/4/2022 21:03:45'!
initializeGives: aCollectionOfDiceThrows 

	collectionOfDiceValues _ ReadStream on: aCollectionOfDiceThrows .
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TestingDice1D12 class' category: 'ISW1-2022-1C-2doParcial'!
TestingDice1D12 class
	instanceVariableNames: ''!

!TestingDice1D12 class methodsFor: 'instance creation' stamp: 'AE 7/4/2022 18:43:52'!
gives: aCollectionOfDiceThrows 
	^self new initializeGives: aCollectionOfDiceThrows ! !
